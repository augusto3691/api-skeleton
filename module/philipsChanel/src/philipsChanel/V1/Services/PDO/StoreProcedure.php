<?php

namespace philipsChanel\V1\Services\PDO;

/**
 * Description of StoreProcedure
 *
 * @author HP
 */
class StoreProcedure
{

    /**
     * @var \Zend\Db\Adapter\Adapter
     */
    private $adapter;

    function __construct(\Zend\Db\Adapter\Adapter $adapter)
    {

        $this->adapter = $adapter;
    }

    public function call($sp, $params, $rowSet = null)
    {
        $stmt = $this->adapter->createStatement('CALL ' . $sp);
        $stmt->execute($params);

        $array = [];
        while ($stmt->getResource()->columnCount() > 0) {
            $array[] = $stmt->getResource()->fetchAll(\PDO::FETCH_ASSOC);
            $stmt->getResource()->nextRowset();
        }

        if (is_null($rowSet)) {
            return end($array);
        } else {
            return $array[$rowSet];
        }
    }

}
