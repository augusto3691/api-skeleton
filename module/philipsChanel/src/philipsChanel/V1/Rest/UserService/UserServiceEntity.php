<?php

namespace philipsChanel\V1\Rest\UserService;

class UserServiceEntity
{

    protected $id_user;
    protected $username;
    protected $first_name;
    protected $last_name;
    protected $id_group;

    function getId_user()
    {
        return $this->id_user;
    }

    function getUsername()
    {
        return $this->username;
    }

    function getFirst_name()
    {
        return $this->first_name;
    }

    function getLast_name()
    {
        return $this->last_name;
    }

    function getId_group()
    {
        return $this->id_group;
    }

    function setId_user($id_user)
    {
        $this->id_user = $id_user;
        return $this;
    }

    function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    function setFirst_name($first_name)
    {
        $this->first_name = $first_name;
        return $this;
    }

    function setLast_name($last_name)
    {
        $this->last_name = $last_name;
        return $this;
    }

    function setId_group($id_group)
    {
        $this->id_group = $id_group;
        return $this;
    }

}
