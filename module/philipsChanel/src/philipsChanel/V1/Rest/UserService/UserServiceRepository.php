<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace philipsChanel\V1\Rest\UserService;

/**
 * Description of UserServiceRepository
 *
 * @author HP
 */
class UserServiceRepository
{

    /**
     * @var \Zend\Db\TableGateway\TableGateway
     */
    private $userTable;

    public function __construct(\Zend\Db\TableGateway\TableGateway $userTable)
    {

        $this->userTable = $userTable;
    }

    public function findAll()
    {
        return $this->userTable->select()->toArray();
    }

}
