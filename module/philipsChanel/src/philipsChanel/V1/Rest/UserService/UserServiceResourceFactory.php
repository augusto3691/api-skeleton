<?php

namespace philipsChanel\V1\Rest\UserService;

class UserServiceResourceFactory
{

    public function __invoke($services)
    {
        return new UserServiceResource($services->get('philipsChanel\\V1\\Rest\\UserService\\UserServiceRepository'));
    }

}
