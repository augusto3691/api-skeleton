<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace philipsChanel\V1\Rest\UserService;

/**
 * Description of UserServiceRepositoryFactory
 *
 * @author HP
 */
class UserServiceRepositoryFactory implements \Zend\ServiceManager\FactoryInterface
{

    public function createService(\Zend\ServiceManager\ServiceLocatorInterface $serviceLocator)
    {
        $tableGateway = $serviceLocator->get('philipsChanel\\V1\\Rest\\UserService\\UserServiceTableGateway');
        return new UserServiceRepository($tableGateway);
    }

}
