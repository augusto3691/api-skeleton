<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace philipsChanel\V1\Rest\UserService;

/**
 * Description of UserServiceTableGatewayFactory
 *
 * @author HP
 */
class UserServiceTableGatewayFactory implements \Zend\ServiceManager\FactoryInterface
{

    public function createService(\Zend\ServiceManager\ServiceLocatorInterface $serviceLocator)
    {
        $adapter = $serviceLocator->get("dbAdapter");

        $hydrator = new \Zend\Db\ResultSet\HydratingResultSet(new \Zend\Stdlib\Hydrator\ClassMethods(FALSE), new UserServiceEntity());

        $tableGateway = new \Zend\Db\TableGateway\TableGateway('oauth_users', $adapter, null, $hydrator);

        return $tableGateway;
    }

}
