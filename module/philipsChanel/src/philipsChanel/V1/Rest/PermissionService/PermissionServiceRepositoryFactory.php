<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace philipsChanel\V1\Rest\PermissionService;

/**
 * Description of PermissionServiceRepositoryFactory
 *
 * @author HP
 */
class PermissionServiceRepositoryFactory implements \Zend\ServiceManager\FactoryInterface
{

    public function createService(\Zend\ServiceManager\ServiceLocatorInterface $serviceLocator)
    {
        $adapter = $serviceLocator->get("dbAdapter");
        $userTableGateway = $serviceLocator->get("philipsChanel\\V1\\Rest\\UserService\\UserServiceTableGateway");

        $resourcesTableGateway = $serviceLocator->get("philipsChanel\\V1\\Rest\\PermissionService\\PermissionServiceResourcesTableGateway");
        $roleGroupTableGateway = $serviceLocator->get("philipsChanel\\V1\\Rest\\PermissionService\\PermissionServiceRoleGroupTableGateway");

        $auth = $serviceLocator->get('api-identity');

        $permissionServiceRepository = new PermissionServiceRepository($userTableGateway, $roleGroupTableGateway, $resourcesTableGateway, $auth, $adapter);

        return $permissionServiceRepository;
    }

}
