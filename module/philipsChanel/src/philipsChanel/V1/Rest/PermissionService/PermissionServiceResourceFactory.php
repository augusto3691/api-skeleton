<?php

namespace philipsChanel\V1\Rest\PermissionService;

class PermissionServiceResourceFactory
{

    public function __invoke($services)
    {
        return new PermissionServiceResource($services->get('philipsChanel\\V1\\Rest\\PermissionService\\PermissionServiceRepository'));
    }

}
