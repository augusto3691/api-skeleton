<?php

namespace philipsChanel\V1\Rest\PermissionService;

class PermissionServiceEntity
{

    protected $id;
    protected $roleGroup;
    protected $resources;

    public function __construct()
    {
        $this->resources = array();
    }

    function getId()
    {
        return $this->id;
    }

    function getRoleGroup()
    {
        return $this->roleGroup;
    }

    function getResources()
    {
        return $this->resources;
    }

    function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    function setRoleGroup($roleGroup)
    {
        $this->roleGroup = $roleGroup;
        return $this;
    }

    function setResource($resource)
    {
        $this->resources[] = $resource;
        return $this;
    }

}
