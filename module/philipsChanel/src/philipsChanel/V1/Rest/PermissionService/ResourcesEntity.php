<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace philipsChanel\V1\Rest\PermissionService;

/**
 * Description of roleGroupEntity
 *
 * @author HP
 */
class ResourcesEntity
{

    protected $id;
    protected $role_group_id;
    protected $permission;
    protected $name;

    function getId()
    {
        return $this->id;
    }

    function getRole_group_id()
    {
        return $this->role_group_id;
    }

    function getPermission()
    {
        return $this->permission;
    }

    function getName()
    {
        return $this->name;
    }

    function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    function setRole_group_id($role_group_id)
    {
        $this->role_group_id = $role_group_id;
        return $this;
    }

    function setPermission($permission)
    {
        $this->permission = $permission;
        return $this;
    }

    function setName($name)
    {
        $this->name = $name;
        return $this;
    }

}
