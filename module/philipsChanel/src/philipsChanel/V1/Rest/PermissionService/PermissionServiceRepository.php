<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace philipsChanel\V1\Rest\PermissionService;

/**
 * Description of PermissionServiceRepository
 *
 * @author HP
 */
class PermissionServiceRepository
{

    private $adapter;

    /**
     * @var \Zend\Db\TableGateway\TableGateway
     */
    private $userTableGateway;
    private $resourcesTableGateway;
    private $roleGroupTableGateway;

    /**
     * @var \ZF\MvcAuth\Identity\AuthenticatedIdentity
     */
    private $auth;

    public function __construct(\Zend\Db\TableGateway\TableGateway $userTableGateway, \Zend\Db\TableGateway\TableGateway $roleGroupTableGateway, \Zend\Db\TableGateway\TableGateway $resourcesTableGateway, \ZF\MvcAuth\Identity\AuthenticatedIdentity $auth, $adapter)
    {
        $this->roleGroupTableGateway = $roleGroupTableGateway;
        $this->resourcesTableGateway = $resourcesTableGateway;
        $this->auth = $auth;
        $this->userTableGateway = $userTableGateway;
        $this->adapter = $adapter;
    }

    public function getUser($username)
    {
        $resultSet = $this->userTableGateway->select(['username' => $username]);
        return $resultSet->current();
    }

    public function getPermissions()
    {
        $username = $this->auth->getRoleId();
        $userId = $this->getUser($username)->getId_user();

        $roleGroup = $this->getRoleGroup($username);
        $resources = $this->getResources($userId);

        $permissions = new PermissionServiceEntity();
        $permissions->setRoleGroup($roleGroup);
        $permissions->setId($userId);

        foreach ($resources as $res) {
            $permissions->setResource($res);
        }

        return $permissions;
    }

    public function getRoleGroup($username)
    {
        return $this->getUser($username)->getId_group();
    }

    public function getResources($userId)
    {
        $sp = new \philipsChanel\V1\Services\PDO\StoreProcedure($this->adapter);
        return $sp->call('sp_get_resources_by_userid (?)', array($userId));
    }

}
