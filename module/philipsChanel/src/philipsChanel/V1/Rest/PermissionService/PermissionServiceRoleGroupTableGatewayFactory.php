<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace philipsChanel\V1\Rest\PermissionService;

/**
 * Description of PermissionServiceRoleGroupTableGatewayFactory
 *
 * @author HP
 */
class PermissionServiceRoleGroupTableGatewayFactory implements \Zend\ServiceManager\FactoryInterface
{

    public function createService(\Zend\ServiceManager\ServiceLocatorInterface $serviceLocator)
    {
        $adapter = $serviceLocator->get("dbAdapter");

        $hydrator = new \Zend\Db\ResultSet\HydratingResultSet(new \Zend\Stdlib\Hydrator\ClassMethods(), new RoleGroupEntity());

        $tableGateway = new \Zend\Db\TableGateway\TableGateway('role_group', $adapter, null, $hydrator);

        return $tableGateway;
    }

}
