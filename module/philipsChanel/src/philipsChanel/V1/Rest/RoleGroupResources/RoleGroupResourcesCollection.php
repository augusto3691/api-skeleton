<?php

namespace philipsChanel\V1\Rest\RoleGroupResources;

use Zend\Paginator\Paginator;

class RoleGroupResourcesCollection extends Paginator
{

    protected $id_group_resource;
    protected $id_group;
    protected $id_resource;
    protected $owner;

    function getId_group_resource()
    {
        return $this->id_group_resource;
    }

    function getId_group()
    {
        return $this->id_group;
    }

    function getId_resource()
    {
        return $this->id_resource;
    }

    function getOwner()
    {
        return $this->owner;
    }

    function setId_group_resource($id_group_resource)
    {
        $this->id_group_resource = $id_group_resource;
        return $this;
    }

    function setId_group($id_group)
    {
        $this->id_group = $id_group;
        return $this;
    }

    function setId_resource($id_resource)
    {
        $this->id_resource = $id_resource;
        return $this;
    }

    function setOwner($owner)
    {
        $this->owner = $owner;
        return $this;
    }

}
