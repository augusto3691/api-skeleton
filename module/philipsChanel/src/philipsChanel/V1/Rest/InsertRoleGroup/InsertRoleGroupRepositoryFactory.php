<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace philipsChanel\V1\Rest\InsertRoleGroup;

/**
 * Description of InsertRoleGroupRepositoryFactory
 *
 * @author HP
 */
class InsertRoleGroupRepositoryFactory implements \Zend\ServiceManager\FactoryInterface
{

    public function createService(\Zend\ServiceManager\ServiceLocatorInterface $serviceLocator)
    {


        $roleGroupTableGateway = $serviceLocator->get('philipsChanel\\V1\\Rest\\RoleGroup\\RoleGroupTableGateway');
        $roleGroupResourcesTableGateway = $serviceLocator->get('philipsChanel\\V1\\Rest\\RoleGroupResources\\RoleGroupResourcesTableGateway');

        return new InsertRoleGroupRepository($roleGroupTableGateway, $roleGroupResourcesTableGateway);
    }

}
