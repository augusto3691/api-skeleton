<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace philipsChanel\V1\Rest\InsertRoleGroup;

/**
 * Description of InsertRoleGroupRepository
 *
 * @author HP
 */
class InsertRoleGroupRepository
{

    /**
     * @var \Zend\Db\TableGateway\TableGateway
     */
    private $roleGroupTableGateway;
    private $roleGroupResourcesTableGateway;

    public function __construct(\Zend\Db\TableGateway\TableGateway $roleGroupTableGateway, \Zend\Db\TableGateway\TableGateway $roleGroupResourcesTableGateway)
    {
        $this->roleGroupTableGateway = $roleGroupTableGateway;
        $this->roleGroupResourcesTableGateway = $roleGroupResourcesTableGateway;
    }

    public function insertRoleGroup($data)
    {

        $hydrator = new \Zend\Stdlib\Hydrator\ObjectProperty();

        $data = $hydrator->extract($data);

        $groupData = $data;
        $resourcesData = $data['resources'];
        $owner = $data['owner'];

        unset($groupData['resources']);

        try {
            $this->roleGroupTableGateway->getAdapter()->getDriver()->getConnection()->beginTransaction();

            $this->roleGroupTableGateway->insert($groupData);
            $id = $this->roleGroupTableGateway->getLastInsertValue();

            $this->insertRoleGroupResource($id, $owner, $resourcesData);

            $this->roleGroupTableGateway->getAdapter()->getDriver()->getConnection()->commit();
            return ['groupID' => $id];
        } catch (Exception $ex) {
            $this->roleGroupTableGateway->getAdapter()->getDriver()->getConnection()->rollback();
        }
    }

    public function insertRoleGroupResource($idGroup, $owner, $resourcesData)
    {

        foreach ($resourcesData as $resourceData) {
            $data = array(
                'id_group' => $idGroup,
                'id_resource' => $resourceData['id_resource'],
                'owner' => $owner
            );
            $this->roleGroupResourcesTableGateway->insert($data);
        }
    }

}
