<?php

namespace philipsChanel\V1\Rest\InsertRoleGroup;

class InsertRoleGroupResourceFactory
{

    public function __invoke($services)
    {
        return new InsertRoleGroupResource($services->get('philipsChanel\\V1\\Rest\\InsertRoleGroup\\InsertRoleGroupRepository'));
    }

}
