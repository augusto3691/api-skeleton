<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace philipsChanel\V1\Rest\SessionService;

/**
 * Description of SessionServiceRepositoryFactory
 *
 * @author HP
 */
class SessionServiceRepositoryFactory implements \Zend\ServiceManager\FactoryInterface
{

    public function createService(\Zend\ServiceManager\ServiceLocatorInterface $serviceLocator)
    {
        $userTableGateway = $serviceLocator->get("philipsChanel\\V1\\Rest\\UserService\\UserServiceTableGateway");
        $auth = $serviceLocator->get('api-identity');

        $sessionEntity = new SessionServiceEntity();

        $sessionRepository = new SessionServiceRepository($auth, $userTableGateway, $sessionEntity);

        return $sessionRepository;
    }

}
