<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace philipsChanel\V1\Rest\SessionService;

/**
 * Description of SessionServiceRepository
 *
 * @author HP
 */
class SessionServiceRepository
{

    /**
     * @var SessionServiceEntity
     */
    private $sessionEntity;

    /**
     * @var \Zend\Db\TableGateway\TableGateway
     */
    private $userTableGateway;
    private $auth;

    public function __construct(\ZF\MvcAuth\Identity\AuthenticatedIdentity $auth, \Zend\Db\TableGateway\TableGateway $userTableGateway, SessionServiceEntity $sessionEntity)
    {

        $this->auth = $auth;
        $this->userTableGateway = $userTableGateway;
        $this->sessionEntity = $sessionEntity;
    }

    public function getSessionData()
    {
        $sessionData = $this->sessionEntity;

        $username = $this->auth->getRoleId();
        $userEntity = $this->userTableGateway->select(['username' => $username]);
        $sessionData->setId($this->auth->getAuthenticationIdentity()['access_token']);
        $sessionData->setUserEntity($userEntity->toArray());

        return $sessionData;
    }

}
