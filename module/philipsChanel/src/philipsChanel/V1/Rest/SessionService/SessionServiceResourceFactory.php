<?php

namespace philipsChanel\V1\Rest\SessionService;

class SessionServiceResourceFactory
{

    public function __invoke($services)
    {
        return new SessionServiceResource($services->get('philipsChanel\\V1\\Rest\\SessionService\\SessionServiceRepository'));
    }

}
