<?php

namespace philipsChanel\V1\Rest\SessionService;

class SessionServiceEntity
{

    protected $id;
    protected $userEntity;

    function getId()
    {
        return $this->id;
    }

    function getUserEntity()
    {
        return $this->userEntity;
    }

    function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    function setUserEntity($userEntity)
    {
        $this->userEntity = $userEntity;
        return $this;
    }

}
