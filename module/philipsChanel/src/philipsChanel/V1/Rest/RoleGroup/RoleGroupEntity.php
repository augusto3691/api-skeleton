<?php

namespace philipsChanel\V1\Rest\RoleGroup;

use ArrayObject;

class RoleGroupEntity extends ArrayObject
{

    protected $id_group;
    protected $groupname;
    protected $owner;

    function getId_group()
    {
        return $this->id_group;
    }

    function getGroupname()
    {
        return $this->groupname;
    }

    function getOwner()
    {
        return $this->owner;
    }

    function setId_group($id_group)
    {
        $this->id_group = $id_group;
        return $this;
    }

    function setGroupname($groupname)
    {
        $this->groupname = $groupname;
        return $this;
    }

    function setOwner($owner)
    {
        $this->owner = $owner;
        return $this;
    }

}
