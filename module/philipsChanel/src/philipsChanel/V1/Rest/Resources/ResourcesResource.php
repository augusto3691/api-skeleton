<?php

namespace philipsChanel\V1\Rest\Resources;

use ZF\Apigility\DbConnectedResource;
use Zend\Paginator\Adapter\DbTableGateway as TableGatewayPaginator;

class ResourcesResource extends DbConnectedResource
{

    public function fetchAll($data = array())
    {
        $hydrator = new \Zend\Stdlib\Hydrator\ObjectProperty();
        $data = $hydrator->extract($data);

        if (!empty($data)) {
            $where = array();
            foreach ($data as $key => $vale) {
                $where[$key] = $vale;
            }

            $adapter = new TableGatewayPaginator($this->table, $where);
        } else {
            $adapter = new TableGatewayPaginator($this->table);
        }
        return new $this->collectionClass($adapter);
    }

}
