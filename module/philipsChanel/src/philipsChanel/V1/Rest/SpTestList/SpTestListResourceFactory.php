<?php

namespace philipsChanel\V1\Rest\SpTestList;

class SpTestListResourceFactory
{

    public function __invoke($services)
    {
        return new SpTestListResource($services->get('philipsChanel\\V1\\Rest\\SpTestList\\SpTestListRepository'));
    }

}
