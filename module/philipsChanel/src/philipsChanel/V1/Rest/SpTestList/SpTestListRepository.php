<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace philipsChanel\V1\Rest\SpTestList;

/**
 * Description of SpTestListRepository
 *
 * @author HP
 */
class SpTestListRepository
{

    /**
     * @var \Zend\Db\Adapter\Adapter
     */
    private $adapter;

    function __construct(\Zend\Db\Adapter\Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function spTestList()
    {
        $sp = new \philipsChanel\V1\Services\PDO\StoreProcedure($this->adapter);

        $paginatorAdapter = new \Zend\Paginator\Adapter\ArrayAdapter($sp->call('sp_TestList (?)', array(NULL)));

        return new SpTestListCollection($paginatorAdapter);
    }

}
