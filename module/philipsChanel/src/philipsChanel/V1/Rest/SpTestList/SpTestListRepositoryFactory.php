<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace philipsChanel\V1\Rest\SpTestList;

/**
 * Description of SpTestListRepositoryFactory
 *
 * @author HP
 */
class SpTestListRepositoryFactory implements \Zend\ServiceManager\FactoryInterface
{

    public function createService(\Zend\ServiceManager\ServiceLocatorInterface $serviceLocator)
    {
        return new SpTestListRepository($serviceLocator->get("dbAdapter"));
    }

}
