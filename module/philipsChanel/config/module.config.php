<?php

return array(
    'service_manager' => array(
        'factories' => array(
            'philipsChanel\\V1\\Rest\\PermissionService\\PermissionServiceResource' => 'philipsChanel\\V1\\Rest\\PermissionService\\PermissionServiceResourceFactory',
            'philipsChanel\\V1\\Rest\\PermissionService\\PermissionServiceResourcesTableGateway' => 'philipsChanel\\V1\\Rest\\PermissionService\\PermissionServiceResourcesTableGatewayFactory',
            'philipsChanel\\V1\\Rest\\PermissionService\\PermissionServiceRoleGroupTableGateway' => 'philipsChanel\\V1\\Rest\\PermissionService\\PermissionServiceRoleGroupTableGatewayFactory',
            'philipsChanel\\V1\\Rest\\PermissionService\\PermissionServiceRepository' => 'philipsChanel\\V1\\Rest\\PermissionService\\PermissionServiceRepositoryFactory',
            'philipsChanel\\V1\\Rest\\UserService\\UserServiceResource' => 'philipsChanel\\V1\\Rest\\UserService\\UserServiceResourceFactory',
            'philipsChanel\\V1\\Rest\\UserService\\UserServiceRepository' => 'philipsChanel\\V1\\Rest\\UserService\\UserServiceRepositoryFactory',
            'philipsChanel\\V1\\Rest\\UserService\\UserServiceTableGateway' => 'philipsChanel\\V1\\Rest\\UserService\\UserServiceTableGatewayFactory',
            'philipsChanel\\V1\\Rest\\SpTestList\\SpTestListResource' => 'philipsChanel\\V1\\Rest\\SpTestList\\SpTestListResourceFactory',
            'philipsChanel\\V1\\Rest\\SpTestList\\SpTestListRepository' => 'philipsChanel\\V1\\Rest\\SpTestList\\SpTestListRepositoryFactory',
            'philipsChanel\\V1\\Rest\\SessionService\\SessionServiceResource' => 'philipsChanel\\V1\\Rest\\SessionService\\SessionServiceResourceFactory',
            'philipsChanel\\V1\\Rest\\SessionService\\SessionServiceRepository' => 'philipsChanel\\V1\\Rest\\SessionService\\SessionServiceRepositoryFactory',
            'philipsChanel\\V1\\Rest\\InsertRoleGroup\\InsertRoleGroupResource' => 'philipsChanel\\V1\\Rest\\InsertRoleGroup\\InsertRoleGroupResourceFactory',
            'philipsChanel\\V1\\Rest\\InsertRoleGroup\\InsertRoleGroupRepository' => 'philipsChanel\\V1\\Rest\\InsertRoleGroup\\InsertRoleGroupRepositoryFactory',
            'philipsChanel\\V1\\Rest\\RoleGroup\\RoleGroupTableGateway' => 'philipsChanel\\V1\\Rest\\RoleGroup\\RoleGroupTableGatewayFactory',
            'philipsChanel\\V1\\Rest\\RoleGroupResources\\RoleGroupResourcesTableGateway' => 'philipsChanel\\V1\\Rest\\RoleGroupResources\\RoleGroupResourcesTableGatewayFactory',
        ),
    ),
    'router' => array(
        'routes' => array(
            'philips-chanel.rest.permission-service' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/permission-service[/:permission_service_id]',
                    'defaults' => array(
                        'controller' => 'philipsChanel\\V1\\Rest\\PermissionService\\Controller',
                    ),
                ),
            ),
            'philips-chanel.rest.user-service' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/user-service[/:user_service_id]',
                    'defaults' => array(
                        'controller' => 'philipsChanel\\V1\\Rest\\UserService\\Controller',
                    ),
                ),
            ),
            'philips-chanel.rest.test-list' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/test_list[/:test_list_id]',
                    'defaults' => array(
                        'controller' => 'philipsChanel\\V1\\Rest\\TestList\\Controller',
                    ),
                ),
            ),
            'philips-chanel.rest.sp-test-list' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/sp-test-list[/:sp_test_list_id]',
                    'defaults' => array(
                        'controller' => 'philipsChanel\\V1\\Rest\\SpTestList\\Controller',
                    ),
                ),
            ),
            'philips-chanel.rest.resources' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/resources[/:resources_id]',
                    'defaults' => array(
                        'controller' => 'philipsChanel\\V1\\Rest\\Resources\\Controller',
                    ),
                ),
            ),
            'philips-chanel.rest.resources-type' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/resources_type[/:resources_type_id]',
                    'defaults' => array(
                        'controller' => 'philipsChanel\\V1\\Rest\\ResourcesType\\Controller',
                    ),
                ),
            ),
            'philips-chanel.rest.session-service' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/session-service[/:session_service_id]',
                    'defaults' => array(
                        'controller' => 'philipsChanel\\V1\\Rest\\SessionService\\Controller',
                    ),
                ),
            ),
            'philips-chanel.rest.log-application' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/log_application[/:log_application_id]',
                    'defaults' => array(
                        'controller' => 'philipsChanel\\V1\\Rest\\LogApplication\\Controller',
                    ),
                ),
            ),
            'philips-chanel.rest.role-group' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/role_group[/:role_group_id]',
                    'defaults' => array(
                        'controller' => 'philipsChanel\\V1\\Rest\\RoleGroup\\Controller',
                    ),
                ),
            ),
            'philips-chanel.rest.role-group-resources' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/role_group_resources[/:role_group_resources_id]',
                    'defaults' => array(
                        'controller' => 'philipsChanel\\V1\\Rest\\RoleGroupResources\\Controller',
                    ),
                ),
            ),
            'philips-chanel.rest.insert-role-group' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/insert-role-group[/:insert_role_group_id]',
                    'defaults' => array(
                        'controller' => 'philipsChanel\\V1\\Rest\\InsertRoleGroup\\Controller',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            0 => 'philips-chanel.rest.permission-service',
            1 => 'philips-chanel.rest.user-service',
            2 => 'philips-chanel.rest.test-list',
            3 => 'philips-chanel.rest.sp-test-list',
            4 => 'philips-chanel.rest.resources',
            5 => 'philips-chanel.rest.resources-type',
            6 => 'philips-chanel.rest.session-service',
            7 => 'philips-chanel.rest.log-application',
            8 => 'philips-chanel.rest.role-group',
            9 => 'philips-chanel.rest.role-group-resources',
            10 => 'philips-chanel.rest.insert-role-group',
        ),
    ),
    'zf-rest' => array(
        'philipsChanel\\V1\\Rest\\PermissionService\\Controller' => array(
            'listener' => 'philipsChanel\\V1\\Rest\\PermissionService\\PermissionServiceResource',
            'route_name' => 'philips-chanel.rest.permission-service',
            'route_identifier_name' => 'permission_service_id',
            'collection_name' => 'permission_service',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => '999',
            'page_size_param' => null,
            'entity_class' => 'philipsChanel\\V1\\Rest\\PermissionService\\PermissionServiceEntity',
            'collection_class' => 'philipsChanel\\V1\\Rest\\PermissionService\\PermissionServiceCollection',
            'service_name' => 'permissionService',
        ),
        'philipsChanel\\V1\\Rest\\UserService\\Controller' => array(
            'listener' => 'philipsChanel\\V1\\Rest\\UserService\\UserServiceResource',
            'route_name' => 'philips-chanel.rest.user-service',
            'route_identifier_name' => 'user_service_id',
            'collection_name' => 'user_service',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => '999',
            'page_size_param' => null,
            'entity_class' => 'philipsChanel\\V1\\Rest\\UserService\\UserServiceEntity',
            'collection_class' => 'philipsChanel\\V1\\Rest\\UserService\\UserServiceCollection',
            'service_name' => 'userService',
        ),
        'philipsChanel\\V1\\Rest\\TestList\\Controller' => array(
            'listener' => 'philipsChanel\\V1\\Rest\\TestList\\TestListResource',
            'route_name' => 'philips-chanel.rest.test-list',
            'route_identifier_name' => 'test_list_id',
            'collection_name' => 'test_list',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => '25',
            'page_size_param' => null,
            'entity_class' => 'philipsChanel\\V1\\Rest\\TestList\\TestListEntity',
            'collection_class' => 'philipsChanel\\V1\\Rest\\TestList\\TestListCollection',
            'service_name' => 'test_list',
        ),
        'philipsChanel\\V1\\Rest\\SpTestList\\Controller' => array(
            'listener' => 'philipsChanel\\V1\\Rest\\SpTestList\\SpTestListResource',
            'route_name' => 'philips-chanel.rest.sp-test-list',
            'route_identifier_name' => 'sp_test_list_id',
            'collection_name' => 'sp_test_list',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => '25',
            'page_size_param' => null,
            'entity_class' => 'philipsChanel\\V1\\Rest\\SpTestList\\SpTestListEntity',
            'collection_class' => 'philipsChanel\\V1\\Rest\\SpTestList\\SpTestListCollection',
            'service_name' => 'spTestList',
        ),
        'philipsChanel\\V1\\Rest\\Resources\\Controller' => array(
            'listener' => 'philipsChanel\\V1\\Rest\\Resources\\ResourcesResource',
            'route_name' => 'philips-chanel.rest.resources',
            'route_identifier_name' => 'resources_id',
            'collection_name' => 'resources',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(
                0 => 'active',
            ),
            'page_size' => '999',
            'page_size_param' => null,
            'entity_class' => 'philipsChanel\\V1\\Rest\\Resources\\ResourcesEntity',
            'collection_class' => 'philipsChanel\\V1\\Rest\\Resources\\ResourcesCollection',
            'service_name' => 'resources',
        ),
        'philipsChanel\\V1\\Rest\\ResourcesType\\Controller' => array(
            'listener' => 'philipsChanel\\V1\\Rest\\ResourcesType\\ResourcesTypeResource',
            'route_name' => 'philips-chanel.rest.resources-type',
            'route_identifier_name' => 'resources_type_id',
            'collection_name' => 'resources_type',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'philipsChanel\\V1\\Rest\\ResourcesType\\ResourcesTypeEntity',
            'collection_class' => 'philipsChanel\\V1\\Rest\\ResourcesType\\ResourcesTypeCollection',
            'service_name' => 'resources_type',
        ),
        'philipsChanel\\V1\\Rest\\SessionService\\Controller' => array(
            'listener' => 'philipsChanel\\V1\\Rest\\SessionService\\SessionServiceResource',
            'route_name' => 'philips-chanel.rest.session-service',
            'route_identifier_name' => 'session_service_id',
            'collection_name' => 'session_service',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'philipsChanel\\V1\\Rest\\SessionService\\SessionServiceEntity',
            'collection_class' => 'philipsChanel\\V1\\Rest\\SessionService\\SessionServiceCollection',
            'service_name' => 'sessionService',
        ),
        'philipsChanel\\V1\\Rest\\LogApplication\\Controller' => array(
            'listener' => 'philipsChanel\\V1\\Rest\\LogApplication\\LogApplicationResource',
            'route_name' => 'philips-chanel.rest.log-application',
            'route_identifier_name' => 'log_application_id',
            'collection_name' => 'log_application',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'philipsChanel\\V1\\Rest\\LogApplication\\LogApplicationEntity',
            'collection_class' => 'philipsChanel\\V1\\Rest\\LogApplication\\LogApplicationCollection',
            'service_name' => 'log_application',
        ),
        'philipsChanel\\V1\\Rest\\RoleGroup\\Controller' => array(
            'listener' => 'philipsChanel\\V1\\Rest\\RoleGroup\\RoleGroupResource',
            'route_name' => 'philips-chanel.rest.role-group',
            'route_identifier_name' => 'role_group_id',
            'collection_name' => 'role_group',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => '999',
            'page_size_param' => null,
            'entity_class' => 'philipsChanel\\V1\\Rest\\RoleGroup\\RoleGroupEntity',
            'collection_class' => 'philipsChanel\\V1\\Rest\\RoleGroup\\RoleGroupCollection',
            'service_name' => 'role_group',
        ),
        'philipsChanel\\V1\\Rest\\RoleGroupResources\\Controller' => array(
            'listener' => 'philipsChanel\\V1\\Rest\\RoleGroupResources\\RoleGroupResourcesResource',
            'route_name' => 'philips-chanel.rest.role-group-resources',
            'route_identifier_name' => 'role_group_resources_id',
            'collection_name' => 'role_group_resources',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => '999',
            'page_size_param' => null,
            'entity_class' => 'philipsChanel\\V1\\Rest\\RoleGroupResources\\RoleGroupResourcesEntity',
            'collection_class' => 'philipsChanel\\V1\\Rest\\RoleGroupResources\\RoleGroupResourcesCollection',
            'service_name' => 'role_group_resources',
        ),
        'philipsChanel\\V1\\Rest\\InsertRoleGroup\\Controller' => array(
            'listener' => 'philipsChanel\\V1\\Rest\\InsertRoleGroup\\InsertRoleGroupResource',
            'route_name' => 'philips-chanel.rest.insert-role-group',
            'route_identifier_name' => 'insert_role_group_id',
            'collection_name' => 'insert_role_group',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'philipsChanel\\V1\\Rest\\InsertRoleGroup\\InsertRoleGroupEntity',
            'collection_class' => 'philipsChanel\\V1\\Rest\\InsertRoleGroup\\InsertRoleGroupCollection',
            'service_name' => 'insertRoleGroup',
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'philipsChanel\\V1\\Rest\\PermissionService\\Controller' => 'HalJson',
            'philipsChanel\\V1\\Rest\\UserService\\Controller' => 'HalJson',
            'philipsChanel\\V1\\Rest\\TestList\\Controller' => 'HalJson',
            'philipsChanel\\V1\\Rest\\SpTestList\\Controller' => 'HalJson',
            'philipsChanel\\V1\\Rest\\Resources\\Controller' => 'HalJson',
            'philipsChanel\\V1\\Rest\\ResourcesType\\Controller' => 'HalJson',
            'philipsChanel\\V1\\Rest\\SessionService\\Controller' => 'HalJson',
            'philipsChanel\\V1\\Rest\\LogApplication\\Controller' => 'HalJson',
            'philipsChanel\\V1\\Rest\\RoleGroup\\Controller' => 'HalJson',
            'philipsChanel\\V1\\Rest\\RoleGroupResources\\Controller' => 'HalJson',
            'philipsChanel\\V1\\Rest\\InsertRoleGroup\\Controller' => 'HalJson',
        ),
        'accept_whitelist' => array(
            'philipsChanel\\V1\\Rest\\PermissionService\\Controller' => array(
                0 => 'application/vnd.philips-chanel.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'philipsChanel\\V1\\Rest\\UserService\\Controller' => array(
                0 => 'application/vnd.philips-chanel.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'philipsChanel\\V1\\Rest\\TestList\\Controller' => array(
                0 => 'application/vnd.philips-chanel.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'philipsChanel\\V1\\Rest\\SpTestList\\Controller' => array(
                0 => 'application/vnd.philips-chanel.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'philipsChanel\\V1\\Rest\\Resources\\Controller' => array(
                0 => 'application/vnd.philips-chanel.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'philipsChanel\\V1\\Rest\\ResourcesType\\Controller' => array(
                0 => 'application/vnd.philips-chanel.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'philipsChanel\\V1\\Rest\\SessionService\\Controller' => array(
                0 => 'application/vnd.philips-chanel.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'philipsChanel\\V1\\Rest\\LogApplication\\Controller' => array(
                0 => 'application/vnd.philips-chanel.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'philipsChanel\\V1\\Rest\\RoleGroup\\Controller' => array(
                0 => 'application/vnd.philips-chanel.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'philipsChanel\\V1\\Rest\\RoleGroupResources\\Controller' => array(
                0 => 'application/vnd.philips-chanel.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'philipsChanel\\V1\\Rest\\InsertRoleGroup\\Controller' => array(
                0 => 'application/vnd.philips-chanel.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
        ),
        'content_type_whitelist' => array(
            'philipsChanel\\V1\\Rest\\PermissionService\\Controller' => array(
                0 => 'application/vnd.philips-chanel.v1+json',
                1 => 'application/json',
            ),
            'philipsChanel\\V1\\Rest\\UserService\\Controller' => array(
                0 => 'application/vnd.philips-chanel.v1+json',
                1 => 'application/json',
            ),
            'philipsChanel\\V1\\Rest\\TestList\\Controller' => array(
                0 => 'application/vnd.philips-chanel.v1+json',
                1 => 'application/json',
            ),
            'philipsChanel\\V1\\Rest\\SpTestList\\Controller' => array(
                0 => 'application/vnd.philips-chanel.v1+json',
                1 => 'application/json',
            ),
            'philipsChanel\\V1\\Rest\\Resources\\Controller' => array(
                0 => 'application/vnd.philips-chanel.v1+json',
                1 => 'application/json',
            ),
            'philipsChanel\\V1\\Rest\\ResourcesType\\Controller' => array(
                0 => 'application/vnd.philips-chanel.v1+json',
                1 => 'application/json',
            ),
            'philipsChanel\\V1\\Rest\\SessionService\\Controller' => array(
                0 => 'application/vnd.philips-chanel.v1+json',
                1 => 'application/json',
            ),
            'philipsChanel\\V1\\Rest\\LogApplication\\Controller' => array(
                0 => 'application/vnd.philips-chanel.v1+json',
                1 => 'application/json',
            ),
            'philipsChanel\\V1\\Rest\\RoleGroup\\Controller' => array(
                0 => 'application/vnd.philips-chanel.v1+json',
                1 => 'application/json',
            ),
            'philipsChanel\\V1\\Rest\\RoleGroupResources\\Controller' => array(
                0 => 'application/vnd.philips-chanel.v1+json',
                1 => 'application/json',
            ),
            'philipsChanel\\V1\\Rest\\InsertRoleGroup\\Controller' => array(
                0 => 'application/vnd.philips-chanel.v1+json',
                1 => 'application/json',
            ),
        ),
    ),
    'zf-hal' => array(
        'metadata_map' => array(
            'philipsChanel\\V1\\Rest\\PermissionService\\PermissionServiceEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'philips-chanel.rest.permission-service',
                'route_identifier_name' => 'permission_service_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ClassMethods',
            ),
            'philipsChanel\\V1\\Rest\\PermissionService\\PermissionServiceCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'philips-chanel.rest.permission-service',
                'route_identifier_name' => 'permission_service_id',
                'is_collection' => true,
            ),
            'philipsChanel\\V1\\Rest\\UserService\\UserServiceEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'philips-chanel.rest.user-service',
                'route_identifier_name' => 'user_service_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ClassMethods',
            ),
            'philipsChanel\\V1\\Rest\\UserService\\UserServiceCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'philips-chanel.rest.user-service',
                'route_identifier_name' => 'user_service_id',
                'is_collection' => true,
            ),
            'philipsChanel\\V1\\Rest\\TestList\\TestListEntity' => array(
                'entity_identifier_name' => 'id_user',
                'route_name' => 'philips-chanel.rest.test-list',
                'route_identifier_name' => 'test_list_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'philipsChanel\\V1\\Rest\\TestList\\TestListCollection' => array(
                'entity_identifier_name' => 'id_user',
                'route_name' => 'philips-chanel.rest.test-list',
                'route_identifier_name' => 'test_list_id',
                'is_collection' => true,
            ),
            'philipsChanel\\V1\\Rest\\SpTestList\\SpTestListEntity' => array(
                'entity_identifier_name' => 'id_user',
                'route_name' => 'philips-chanel.rest.sp-test-list',
                'route_identifier_name' => 'sp_test_list_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'philipsChanel\\V1\\Rest\\SpTestList\\SpTestListCollection' => array(
                'entity_identifier_name' => 'id_user',
                'route_name' => 'philips-chanel.rest.sp-test-list',
                'route_identifier_name' => 'sp_test_list_id',
                'is_collection' => true,
            ),
            'philipsChanel\\V1\\Rest\\Resources\\ResourcesEntity' => array(
                'entity_identifier_name' => 'id_resource',
                'route_name' => 'philips-chanel.rest.resources',
                'route_identifier_name' => 'resources_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'philipsChanel\\V1\\Rest\\Resources\\ResourcesCollection' => array(
                'entity_identifier_name' => 'id_resource',
                'route_name' => 'philips-chanel.rest.resources',
                'route_identifier_name' => 'resources_id',
                'is_collection' => true,
            ),
            'philipsChanel\\V1\\Rest\\ResourcesType\\ResourcesTypeEntity' => array(
                'entity_identifier_name' => 'id_type',
                'route_name' => 'philips-chanel.rest.resources-type',
                'route_identifier_name' => 'resources_type_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'philipsChanel\\V1\\Rest\\ResourcesType\\ResourcesTypeCollection' => array(
                'entity_identifier_name' => 'id_type',
                'route_name' => 'philips-chanel.rest.resources-type',
                'route_identifier_name' => 'resources_type_id',
                'is_collection' => true,
            ),
            'philipsChanel\\V1\\Rest\\SessionService\\SessionServiceEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'philips-chanel.rest.session-service',
                'route_identifier_name' => 'session_service_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ClassMethods',
            ),
            'philipsChanel\\V1\\Rest\\SessionService\\SessionServiceCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'philips-chanel.rest.session-service',
                'route_identifier_name' => 'session_service_id',
                'is_collection' => true,
            ),
            'philipsChanel\\V1\\Rest\\LogApplication\\LogApplicationEntity' => array(
                'entity_identifier_name' => 'id_logapp',
                'route_name' => 'philips-chanel.rest.log-application',
                'route_identifier_name' => 'log_application_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'philipsChanel\\V1\\Rest\\LogApplication\\LogApplicationCollection' => array(
                'entity_identifier_name' => 'id_logapp',
                'route_name' => 'philips-chanel.rest.log-application',
                'route_identifier_name' => 'log_application_id',
                'is_collection' => true,
            ),
            'philipsChanel\\V1\\Rest\\RoleGroup\\RoleGroupEntity' => array(
                'entity_identifier_name' => 'id_group',
                'route_name' => 'philips-chanel.rest.role-group',
                'route_identifier_name' => 'role_group_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'philipsChanel\\V1\\Rest\\RoleGroup\\RoleGroupCollection' => array(
                'entity_identifier_name' => 'id_group',
                'route_name' => 'philips-chanel.rest.role-group',
                'route_identifier_name' => 'role_group_id',
                'is_collection' => true,
            ),
            'philipsChanel\\V1\\Rest\\RoleGroupResources\\RoleGroupResourcesEntity' => array(
                'entity_identifier_name' => 'id_group_resource',
                'route_name' => 'philips-chanel.rest.role-group-resources',
                'route_identifier_name' => 'role_group_resources_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'philipsChanel\\V1\\Rest\\RoleGroupResources\\RoleGroupResourcesCollection' => array(
                'entity_identifier_name' => 'id_group_resource',
                'route_name' => 'philips-chanel.rest.role-group-resources',
                'route_identifier_name' => 'role_group_resources_id',
                'is_collection' => true,
            ),
            'philipsChanel\\V1\\Rest\\InsertRoleGroup\\InsertRoleGroupEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'philips-chanel.rest.insert-role-group',
                'route_identifier_name' => 'insert_role_group_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ClassMethods',
            ),
            'philipsChanel\\V1\\Rest\\InsertRoleGroup\\InsertRoleGroupCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'philips-chanel.rest.insert-role-group',
                'route_identifier_name' => 'insert_role_group_id',
                'is_collection' => true,
            ),
        ),
    ),
    'zf-apigility' => array(
        'db-connected' => array(
            'philipsChanel\\V1\\Rest\\TestList\\TestListResource' => array(
                'adapter_name' => 'dbAdapter',
                'table_name' => 'test_list',
                'hydrator_name' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
                'controller_service_name' => 'philipsChanel\\V1\\Rest\\TestList\\Controller',
                'entity_identifier_name' => 'id_user',
                'table_service' => 'philipsChanel\\V1\\Rest\\TestList\\TestListResource\\Table',
            ),
            'philipsChanel\\V1\\Rest\\Resources\\ResourcesResource' => array(
                'adapter_name' => 'dbAdapter',
                'table_name' => 'resources',
                'hydrator_name' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
                'controller_service_name' => 'philipsChanel\\V1\\Rest\\Resources\\Controller',
                'entity_identifier_name' => 'id_resource',
                'table_service' => 'philipsChanel\\V1\\Rest\\Resources\\ResourcesResource\\Table',
                'resource_class' => 'philipsChanel\\V1\\Rest\\Resources\\ResourcesResource',
            ),
            'philipsChanel\\V1\\Rest\\ResourcesType\\ResourcesTypeResource' => array(
                'adapter_name' => 'dbAdapter',
                'table_name' => 'resources_type',
                'hydrator_name' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
                'controller_service_name' => 'philipsChanel\\V1\\Rest\\ResourcesType\\Controller',
                'entity_identifier_name' => 'id_type',
                'table_service' => 'philipsChanel\\V1\\Rest\\ResourcesType\\ResourcesTypeResource\\Table',
            ),
            'philipsChanel\\V1\\Rest\\LogApplication\\LogApplicationResource' => array(
                'adapter_name' => 'dbAdapter',
                'table_name' => 'log_application',
                'hydrator_name' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
                'controller_service_name' => 'philipsChanel\\V1\\Rest\\LogApplication\\Controller',
                'entity_identifier_name' => 'id_logapp',
                'table_service' => 'philipsChanel\\V1\\Rest\\LogApplication\\LogApplicationResource\\Table',
            ),
            'philipsChanel\\V1\\Rest\\RoleGroup\\RoleGroupResource' => array(
                'adapter_name' => 'dbAdapter',
                'table_name' => 'role_group',
                'hydrator_name' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
                'controller_service_name' => 'philipsChanel\\V1\\Rest\\RoleGroup\\Controller',
                'entity_identifier_name' => 'id_group',
                'table_service' => 'philipsChanel\\V1\\Rest\\RoleGroup\\RoleGroupResource\\Table',
            ),
            'philipsChanel\\V1\\Rest\\RoleGroupResources\\RoleGroupResourcesResource' => array(
                'adapter_name' => 'dbAdapter',
                'table_name' => 'role_group_resources',
                'hydrator_name' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
                'controller_service_name' => 'philipsChanel\\V1\\Rest\\RoleGroupResources\\Controller',
                'entity_identifier_name' => 'id_group_resource',
                'table_service' => 'philipsChanel\\V1\\Rest\\RoleGroupResources\\RoleGroupResourcesResource\\Table',
            ),
        ),
    ),
    'zf-content-validation' => array(
        'philipsChanel\\V1\\Rest\\Resources\\Controller' => array(
            'input_filter' => 'philipsChanel\\V1\\Rest\\Resources\\Validator',
        ),
        'philipsChanel\\V1\\Rest\\ResourcesType\\Controller' => array(
            'input_filter' => 'philipsChanel\\V1\\Rest\\ResourcesType\\Validator',
        ),
        'philipsChanel\\V1\\Rest\\LogApplication\\Controller' => array(
            'input_filter' => 'philipsChanel\\V1\\Rest\\LogApplication\\Validator',
        ),
        'philipsChanel\\V1\\Rest\\RoleGroup\\Controller' => array(
            'input_filter' => 'philipsChanel\\V1\\Rest\\RoleGroup\\Validator',
        ),
        'philipsChanel\\V1\\Rest\\RoleGroupResources\\Controller' => array(
            'input_filter' => 'philipsChanel\\V1\\Rest\\RoleGroupResources\\Validator',
        ),
    ),
    'input_filter_specs' => array(
        'philipsChanel\\V1\\Rest\\TestList\\Validator' => array(
            0 => array(
                'name' => 'name',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => '255',
                        ),
                    ),
                ),
            ),
            1 => array(
                'name' => 'number',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => '255',
                        ),
                    ),
                ),
            ),
            2 => array(
                'name' => 'custom',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => '255',
                        ),
                    ),
                ),
            ),
        ),
        'philipsChanel\\V1\\Rest\\VwAllResources\\Validator' => array(
            0 => array(
                'name' => 'id_resource',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\Digits',
                    ),
                ),
                'validators' => array(),
            ),
            1 => array(
                'name' => 'resourcename',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => '100',
                        ),
                    ),
                ),
            ),
            2 => array(
                'name' => 'resourcedesc',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => '255',
                        ),
                    ),
                ),
            ),
            3 => array(
                'name' => 'created_on',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
            ),
            4 => array(
                'name' => 'updated_on',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
            ),
            5 => array(
                'name' => 'owner',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => '255',
                        ),
                    ),
                ),
            ),
            6 => array(
                'name' => 'type_name',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => '20',
                        ),
                    ),
                ),
            ),
        ),
        'philipsChanel\\V1\\Rest\\Resources\\Validator' => array(
            0 => array(
                'name' => 'resource_name',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'ZF\\ContentValidation\\Validator\\DbNoRecordExists',
                        'options' => array(
                            'adapter' => 'dbAdapter',
                            'table' => 'resources',
                            'field' => 'resource_name',
                        ),
                    ),
                    1 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => '100',
                        ),
                    ),
                ),
            ),
            1 => array(
                'name' => 'resource_desc',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => '255',
                        ),
                    ),
                ),
            ),
            2 => array(
                'name' => 'owner',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => '255',
                        ),
                    ),
                ),
            ),
            3 => array(
                'name' => 'id_type',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'ZF\\ContentValidation\\Validator\\DbRecordExists',
                        'options' => array(
                            'adapter' => 'dbAdapter',
                            'table' => 'resources_type',
                            'field' => 'id_type',
                        ),
                    ),
                    1 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => '20',
                        ),
                    ),
                ),
            ),
            4 => array(
                'required' => false,
                'validators' => array(),
                'filters' => array(),
                'name' => 'active',
            ),
        ),
        'philipsChanel\\V1\\Rest\\ResourcesType\\Validator' => array(
            0 => array(
                'name' => 'type_name',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'ZF\\ContentValidation\\Validator\\DbNoRecordExists',
                        'options' => array(
                            'adapter' => 'dbAdapter',
                            'table' => 'resources_type',
                            'field' => 'type_name',
                        ),
                    ),
                    1 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => '20',
                        ),
                    ),
                ),
            ),
        ),
        'philipsChanel\\V1\\Rest\\LogApplication\\Validator' => array(
            0 => array(
                'name' => 'user',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => '254',
                        ),
                    ),
                ),
            ),
            1 => array(
                'name' => 'action',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => '1000',
                        ),
                    ),
                ),
            ),
            2 => array(
                'name' => 'entity',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => '1000',
                        ),
                    ),
                ),
            ),
            3 => array(
                'required' => false,
                'validators' => array(),
                'filters' => array(),
                'name' => 'sniffer',
            ),
            4 => array(
                'required' => true,
                'validators' => array(),
                'filters' => array(),
                'name' => 'id_log_type',
            ),
        ),
        'philipsChanel\\V1\\Rest\\RoleGroup\\Validator' => array(
            0 => array(
                'name' => 'groupname',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'ZF\\ContentValidation\\Validator\\DbNoRecordExists',
                        'options' => array(
                            'adapter' => 'dbAdapter',
                            'table' => 'role_group',
                            'field' => 'groupname',
                        ),
                    ),
                    1 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => '80',
                        ),
                    ),
                ),
            ),
            1 => array(
                'name' => 'created_on',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
            ),
            2 => array(
                'name' => 'updated_on',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
            ),
            3 => array(
                'name' => 'owner',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => '255',
                        ),
                    ),
                ),
            ),
        ),
        'philipsChanel\\V1\\Rest\\RoleGroupResources\\Validator' => array(
            0 => array(
                'name' => 'id_group',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\Digits',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'ZF\\ContentValidation\\Validator\\DbNoRecordExists',
                        'options' => array(
                            'adapter' => 'dbAdapter',
                            'table' => 'role_group_resources',
                            'field' => 'id_group',
                        ),
                    ),
                    1 => array(
                        'name' => 'ZF\\ContentValidation\\Validator\\DbRecordExists',
                        'options' => array(
                            'adapter' => 'dbAdapter',
                            'table' => null,
                            'field' => null,
                        ),
                    ),
                ),
            ),
            1 => array(
                'name' => 'id_resource',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\Digits',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'ZF\\ContentValidation\\Validator\\DbNoRecordExists',
                        'options' => array(
                            'adapter' => 'dbAdapter',
                            'table' => 'role_group_resources',
                            'field' => 'id_resource',
                        ),
                    ),
                    1 => array(
                        'name' => 'ZF\\ContentValidation\\Validator\\DbRecordExists',
                        'options' => array(
                            'adapter' => 'dbAdapter',
                            'table' => null,
                            'field' => null,
                        ),
                    ),
                ),
            ),
            2 => array(
                'name' => 'created_on',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
            ),
            3 => array(
                'name' => 'updated_on',
                'required' => true,
                'filters' => array(),
                'validators' => array(),
            ),
            4 => array(
                'name' => 'owner',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => 1,
                            'max' => '255',
                        ),
                    ),
                ),
            ),
        ),
    ),
    'zf-mvc-auth' => array(
        'authorization' => array(
            'philipsChanel\\V1\\Rest\\TestList\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
            'philipsChanel\\V1\\Rest\\SessionService\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
            'philipsChanel\\V1\\Rest\\Resources\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => true,
                    'DELETE' => false,
                ),
            ),
        ),
    ),
);
