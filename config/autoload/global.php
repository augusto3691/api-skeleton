<?php
return array(
    'db' => array(
        'adapters' => array(
            'dbAdapter' => array(
                'charset' => 'utf8',
                'database' => 'philips_dv',
                'driver' => 'PDO_Mysql',
                'hostname' => 'localhost',
                'username' => 'bd_trend2b_sa',
                'password' => 'trend81sa',
            ),
        ),
    ),
    'router' => array(
        'routes' => array(
            'oauth' => array(
                'options' => array(
                    'spec' => '%oauth%',
                    'regex' => '(?P<oauth>(/oauth))',
                ),
                'type' => 'regex',
            ),
        ),
    ),
    'zf-mvc-auth' => array(
        'authentication' => array(
            'adapters' => array(
                'oauthadapterdev' => array(
                    'adapter' => 'ZF\\MvcAuth\\Authentication\\OAuth2Adapter',
                    'storage' => array(
                        'adapter' => 'pdo',
                        'dsn' => 'mysql:host=localhost;dbname=philips_dv',
                        'route' => '/oauth',
                        'username' => 'bd_trend2b_sa',
                        'password' => 'trend81sa',
                    ),
                ),
            ),
            'map' => array(
                'philipsChanel\\V1' => 'oauthadapterdev',
            ),
        ),
    )
);
