CREATE DATABASE  IF NOT EXISTS `philips_dv` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;
USE `philips_dv`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 54.242.144.150    Database: philips_dv
-- ------------------------------------------------------
-- Server version	5.7.10-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `log_application`
--

DROP TABLE IF EXISTS `log_application`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_application` (
  `id_logapp` int(100) NOT NULL AUTO_INCREMENT,
  `user` varchar(254) COLLATE utf8_bin NOT NULL,
  `action` varchar(1000) COLLATE utf8_bin NOT NULL,
  `entity` varchar(1000) COLLATE utf8_bin NOT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_log_type` int(10) NOT NULL,
  `sniffer` longtext CHARACTER SET utf8,
  PRIMARY KEY (`id_logapp`),
  KEY `fk_id_log_type_idx` (`id_log_type`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_application`
--

LOCK TABLES `log_application` WRITE;
/*!40000 ALTER TABLE `log_application` DISABLE KEYS */;
INSERT INTO `log_application` VALUES (1,'sergio@trend2b.com','O usuário sergio@trend2b.com entrou no sistema','oauth_users','2016-01-22 17:39:27',1,'{\n  \"_links\": {\n    \"self\": {\n      \"href\": \"http://philips-api.local/log_application?page=1\"\n    },\n    \"first\": {\n      \"href\": \"http://philips-api.local/log_application\"\n    },\n    \"last\": {\n      \"href\": \"http://philips-api.local/log_application?page=1\"\n    }\n  },\n  \"_embedded\": {\n    \"log_application\": [\n      {\n        \"id_logapp\": \"1\",\n        \"user\": \"sergio@trend2b.com\",\n        \"action\": \"O usuário sergio@trend2b.com entrou no sistema\",\n        \"entity\": \"oauth_users\",\n        \"created_on\": \"2016-01-22 17:39:27\",\n        \"id_log_type\": \"1\",\n        \"_links\": {\n          \"self\": {\n            \"href\": \"http://philips-api.local/log_application/1\"\n          }\n        }\n      }\n    ]\n  },\n  \"page_count\": 1,\n  \"page_size\": 25,\n  \"total_items\": 1,\n  \"page\": 1\n}'),(2,'sergio@trend2b.com','O usuário inseriu uma nova permissão','resources','2016-01-26 09:49:13',1,NULL),(3,'sergio@trend2b.com','O usuário inseriu uma nova permissão','resources','2016-01-26 09:51:24',1,NULL),(4,'sergio@trend2b.com','O usuario inseriu uma nova permissão','resources','2016-01-26 09:52:30',1,NULL),(5,'sergio@trend2b.com','O usuário inseriu uma nova permissão','resources','2016-01-26 09:55:48',1,'{\"data\":{\"id_resource\":\"31\",\"resource_name\":\"4563122312312313123\",\"resource_desc\":\"3122132133246549879\",\"created_on\":\"2016-01-26 09:55:46\",\"updated_on\":\"2016-01-26 09:55:46\",\"owner\":\"sergio@trend2b.com\",\"id_type\":\"3\",\"active\":\"1\",\"_links\":{\"self\":{\"href\":\"http://philips-api.local/resources/31\"}}},\"status\":201,\"config\":{\"method\":\"POST\",\"transformRequest\":[null],\"transformResponse\":[null],\"url\":\"http://philips-api.local/resources\",\"data\":{\"resource_name\":\"4563122312312313123\",\"resource_desc\":\"3122132133246549879\",\"owner\":\"sergio@trend2b.com\",\"id_type\":\"3\"},\"headers\":{\"Accept\":\"application/json, text/plain, */*\",\"Content-Type\":\"application/json;charset=utf-8\",\"Authorization\":\"Bearer 168ee0a63b48c1adab2613b611d1f751c66b6787\"}},\"statusText\":\"Created\"}'),(6,'sergio@trend2b.com','O usuário inseriu uma nova permissão','resources','2016-01-26 09:56:40',1,'{\"data\":{\"id_resource\":\"32\",\"resource_name\":\"123456789\",\"resource_desc\":\"463789546\",\"created_on\":\"2016-01-26 09:56:37\",\"updated_on\":\"2016-01-26 09:56:37\",\"owner\":\"sergio@trend2b.com\",\"id_type\":\"1\",\"active\":\"1\",\"_links\":{\"self\":{\"href\":\"http://philips-api.local/resources/32\"}}},\"status\":201,\"config\":{\"method\":\"POST\",\"transformRequest\":[null],\"transformResponse\":[null],\"url\":\"http://philips-api.local/resources\",\"data\":{\"resource_name\":\"123456789\",\"resource_desc\":\"463789546\",\"owner\":\"sergio@trend2b.com\",\"id_type\":\"1\"},\"headers\":{\"Accept\":\"application/json, text/plain, */*\",\"Content-Type\":\"application/json;charset=utf-8\",\"Authorization\":\"Bearer 168ee0a63b48c1adab2613b611d1f751c66b6787\"}},\"statusText\":\"Created\"}'),(7,'sergio@trend2b.com','O usuário inseriu uma nova permissão','resources','2016-01-26 09:59:48',1,'{\"data\":{\"id_resource\":\"33\",\"resource_name\":\"adfefddf\",\"resource_desc\":\"dfdfddsdfdfdf\",\"created_on\":\"2016-01-26 09:59:46\",\"updated_on\":\"2016-01-26 09:59:46\",\"owner\":\"sergio@trend2b.com\",\"id_type\":\"1\",\"active\":\"1\",\"_links\":{\"self\":{\"href\":\"http://philips-api.local/resources/33\"}}},\"status\":201,\"config\":{\"method\":\"POST\",\"transformRequest\":[null],\"transformResponse\":[null],\"url\":\"http://philips-api.local/resources\",\"data\":{\"resource_name\":\"adfefddf\",\"resource_desc\":\"dfdfddsdfdfdf\",\"owner\":\"sergio@trend2b.com\",\"id_type\":\"1\"},\"headers\":{\"Accept\":\"application/json, text/plain, */*\",\"Content-Type\":\"application/json;charset=utf-8\",\"Authorization\":\"Bearer 168ee0a63b48c1adab2613b611d1f751c66b6787\"}},\"statusText\":\"Created\"}'),(8,'sergio@trend2b.com','O usuário inseriu uma nova permissão','resources','2016-01-26 10:08:07',1,'{\"data\":{\"id_resource\":\"34\",\"resource_name\":\"89f897df987df897df987\",\"resource_desc\":\"978df897sdf89sd987sdf897\",\"created_on\":\"2016-01-26 10:08:04\",\"updated_on\":\"2016-01-26 10:08:04\",\"owner\":\"sergio@trend2b.com\",\"id_type\":\"3\",\"active\":\"1\",\"_links\":{\"self\":{\"href\":\"http://philips-api.local/resources/34\"}}},\"status\":201,\"config\":{\"method\":\"POST\",\"transformRequest\":[null],\"transformResponse\":[null],\"url\":\"http://philips-api.local/resources\",\"data\":{\"resource_name\":\"89f897df987df897df987\",\"resource_desc\":\"978df897sdf89sd987sdf897\",\"owner\":\"sergio@trend2b.com\",\"id_type\":\"3\"},\"headers\":{\"Accept\":\"application/json, text/plain, */*\",\"Content-Type\":\"application/json;charset=utf-8\",\"Authorization\":\"Bearer 168ee0a63b48c1adab2613b611d1f751c66b6787\"}},\"statusText\":\"Created\"}'),(9,'sergio@trend2b.com','Ocorreu um erro ao inserir a permissão','resources','2016-01-26 10:09:08',1,'{\"data\":{\"validation_messages\":{\"resource_name\":{\"recordFound\":\"A record matching the input was found\"}},\"type\":\"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\"title\":\"Unprocessable Entity\",\"status\":422,\"detail\":\"Failed Validation\"},\"status\":422,\"config\":{\"method\":\"POST\",\"transformRequest\":[null],\"transformResponse\":[null],\"url\":\"http://philips-api.local/resources\",\"data\":{\"resource_name\":\"teste\",\"resource_desc\":\"teste de insert\",\"owner\":\"sergio@trend2b.com\",\"id_type\":\"4\"},\"headers\":{\"Accept\":\"application/json, text/plain, */*\",\"Content-Type\":\"application/json;charset=utf-8\",\"Authorization\":\"Bearer 168ee0a63b48c1adab2613b611d1f751c66b6787\"}},\"statusText\":\"Unprocessable Entity\"}'),(10,'sergio@trend2b.com','Ocorreu um erro ao inserir a permissão','resources','2016-01-26 10:39:32',2,'{\"data\":{\"trace\":[{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zendframework\\\\zend-db\\\\src\\\\TableGateway\\\\AbstractTableGateway.php\",\"line\":307,\"function\":\"execute\",\"class\":\"Zend\\\\Db\\\\Adapter\\\\Driver\\\\Pdo\\\\Statement\",\"type\":\"->\",\"args\":[]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zendframework\\\\zend-db\\\\src\\\\TableGateway\\\\AbstractTableGateway.php\",\"line\":263,\"function\":\"executeInsert\",\"class\":\"Zend\\\\Db\\\\TableGateway\\\\AbstractTableGateway\",\"type\":\"->\",\"args\":[{}]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zfcampus\\\\zf-apigility\\\\src\\\\DbConnectedResource.php\",\"line\":31,\"function\":\"insert\",\"class\":\"Zend\\\\Db\\\\TableGateway\\\\AbstractTableGateway\",\"type\":\"->\",\"args\":[{\"resource_name\":\"assdfsfad\",\"resource_desc\":\"asdsadfsadfsadf\",\"owner\":\"sergio@trend2b.com\",\"id_type\":\"4\"}]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zfcampus\\\\zf-rest\\\\src\\\\AbstractResourceListener.php\",\"line\":157,\"function\":\"create\",\"class\":\"ZF\\\\Apigility\\\\DbConnectedResource\",\"type\":\"->\",\"args\":[{\"resource_name\":\"assdfsfad\",\"resource_desc\":\"asdsadfsadfsadf\",\"owner\":\"sergio@trend2b.com\",\"id_type\":\"4\"}]},{\"function\":\"dispatch\",\"class\":\"ZF\\\\Rest\\\\AbstractResourceListener\",\"type\":\"->\",\"args\":[{}]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zendframework\\\\zend-eventmanager\\\\src\\\\EventManager.php\",\"line\":444,\"function\":\"call_user_func\",\"args\":[[{},\"dispatch\"],{}]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zendframework\\\\zend-eventmanager\\\\src\\\\EventManager.php\",\"line\":205,\"function\":\"triggerListeners\",\"class\":\"Zend\\\\EventManager\\\\EventManager\",\"type\":\"->\",\"args\":[\"create\",{},{}]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zfcampus\\\\zf-rest\\\\src\\\\Resource.php\",\"line\":538,\"function\":\"trigger\",\"class\":\"Zend\\\\EventManager\\\\EventManager\",\"type\":\"->\",\"args\":[{},{}]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zfcampus\\\\zf-rest\\\\src\\\\Resource.php\",\"line\":237,\"function\":\"triggerEvent\",\"class\":\"ZF\\\\Rest\\\\Resource\",\"type\":\"->\",\"args\":[\"create\",{\"data\":{\"resource_name\":\"assdfsfad\",\"resource_desc\":\"asdsadfsadfsadf\",\"owner\":\"sergio@trend2b.com\",\"id_type\":\"4\"}}]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zfcampus\\\\zf-rest\\\\src\\\\RestController.php\",\"line\":370,\"function\":\"create\",\"class\":\"ZF\\\\Rest\\\\Resource\",\"type\":\"->\",\"args\":[{\"resource_name\":\"assdfsfad\",\"resource_desc\":\"asdsadfsadfsadf\",\"owner\":\"sergio@trend2b.com\",\"id_type\":\"4\"}]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zfcampus\\\\zf-rest\\\\src\\\\RestController.php\",\"line\":889,\"function\":\"create\",\"class\":\"ZF\\\\Rest\\\\RestController\",\"type\":\"->\",\"args\":[{\"resource_name\":\"assdfsfad\",\"resource_desc\":\"asdsadfsadfsadf\",\"owner\":\"sergio@trend2b.com\",\"id_type\":\"4\"}]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zendframework\\\\zend-mvc\\\\src\\\\Controller\\\\AbstractRestfulController.php\",\"line\":416,\"function\":\"processPostData\",\"class\":\"ZF\\\\Rest\\\\RestController\",\"type\":\"->\",\"args\":[{}]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zfcampus\\\\zf-rest\\\\src\\\\RestController.php\",\"line\":333,\"function\":\"onDispatch\",\"class\":\"Zend\\\\Mvc\\\\Controller\\\\AbstractRestfulController\",\"type\":\"->\",\"args\":[{}]},{\"function\":\"onDispatch\",\"class\":\"ZF\\\\Rest\\\\RestController\",\"type\":\"->\",\"args\":[{}]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zendframework\\\\zend-eventmanager\\\\src\\\\EventManager.php\",\"line\":444,\"function\":\"call_user_func\",\"args\":[[{},\"onDispatch\"],{}]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zendframework\\\\zend-eventmanager\\\\src\\\\EventManager.php\",\"line\":205,\"function\":\"triggerListeners\",\"class\":\"Zend\\\\EventManager\\\\EventManager\",\"type\":\"->\",\"args\":[\"dispatch\",{},{}]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zendframework\\\\zend-mvc\\\\src\\\\Controller\\\\AbstractController.php\",\"line\":118,\"function\":\"trigger\",\"class\":\"Zend\\\\EventManager\\\\EventManager\",\"type\":\"->\",\"args\":[\"dispatch\",{},{}]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zendframework\\\\zend-mvc\\\\src\\\\Controller\\\\AbstractRestfulController.php\",\"line\":300,\"function\":\"dispatch\",\"class\":\"Zend\\\\Mvc\\\\Controller\\\\AbstractController\",\"type\":\"->\",\"args\":[{},{}]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zendframework\\\\zend-mvc\\\\src\\\\DispatchListener.php\",\"line\":93,\"function\":\"dispatch\",\"class\":\"Zend\\\\Mvc\\\\Controller\\\\AbstractRestfulController\",\"type\":\"->\",\"args\":[{},{}]},{\"function\":\"onDispatch\",\"class\":\"Zend\\\\Mvc\\\\DispatchListener\",\"type\":\"->\",\"args\":[{}]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zendframework\\\\zend-eventmanager\\\\src\\\\EventManager.php\",\"line\":444,\"function\":\"call_user_func\",\"args\":[[{},\"onDispatch\"],{}]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zendframework\\\\zend-eventmanager\\\\src\\\\EventManager.php\",\"line\":205,\"function\":\"triggerListeners\",\"class\":\"Zend\\\\EventManager\\\\EventManager\",\"type\":\"->\",\"args\":[\"dispatch\",{},{}]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zendframework\\\\zend-mvc\\\\src\\\\Application.php\",\"line\":314,\"function\":\"trigger\",\"class\":\"Zend\\\\EventManager\\\\EventManager\",\"type\":\"->\",\"args\":[\"dispatch\",{},{}]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\public\\\\index.php\",\"line\":51,\"function\":\"run\",\"class\":\"Zend\\\\Mvc\\\\Application\",\"type\":\"->\",\"args\":[]}],\"exception_stack\":[{\"code\":0,\"message\":\"SQLSTATE[HY000]: General error: 1364 Field \'teste\' doesn\'t have a default value\",\"trace\":[{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zendframework\\\\zend-db\\\\src\\\\Adapter\\\\Driver\\\\Pdo\\\\Statement.php\",\"line\":239,\"function\":\"execute\",\"class\":\"PDOStatement\",\"type\":\"->\",\"args\":[]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zendframework\\\\zend-db\\\\src\\\\TableGateway\\\\AbstractTableGateway.php\",\"line\":307,\"function\":\"execute\",\"class\":\"Zend\\\\Db\\\\Adapter\\\\Driver\\\\Pdo\\\\Statement\",\"type\":\"->\",\"args\":[]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zendframework\\\\zend-db\\\\src\\\\TableGateway\\\\AbstractTableGateway.php\",\"line\":263,\"function\":\"executeInsert\",\"class\":\"Zend\\\\Db\\\\TableGateway\\\\AbstractTableGateway\",\"type\":\"->\",\"args\":[{}]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zfcampus\\\\zf-apigility\\\\src\\\\DbConnectedResource.php\",\"line\":31,\"function\":\"insert\",\"class\":\"Zend\\\\Db\\\\TableGateway\\\\AbstractTableGateway\",\"type\":\"->\",\"args\":[{\"resource_name\":\"assdfsfad\",\"resource_desc\":\"asdsadfsadfsadf\",\"owner\":\"sergio@trend2b.com\",\"id_type\":\"4\"}]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zfcampus\\\\zf-rest\\\\src\\\\AbstractResourceListener.php\",\"line\":157,\"function\":\"create\",\"class\":\"ZF\\\\Apigility\\\\DbConnectedResource\",\"type\":\"->\",\"args\":[{\"resource_name\":\"assdfsfad\",\"resource_desc\":\"asdsadfsadfsadf\",\"owner\":\"sergio@trend2b.com\",\"id_type\":\"4\"}]},{\"function\":\"dispatch\",\"class\":\"ZF\\\\Rest\\\\AbstractResourceListener\",\"type\":\"->\",\"args\":[{}]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zendframework\\\\zend-eventmanager\\\\src\\\\EventManager.php\",\"line\":444,\"function\":\"call_user_func\",\"args\":[[{},\"dispatch\"],{}]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zendframework\\\\zend-eventmanager\\\\src\\\\EventManager.php\",\"line\":205,\"function\":\"triggerListeners\",\"class\":\"Zend\\\\EventManager\\\\EventManager\",\"type\":\"->\",\"args\":[\"create\",{},{}]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zfcampus\\\\zf-rest\\\\src\\\\Resource.php\",\"line\":538,\"function\":\"trigger\",\"class\":\"Zend\\\\EventManager\\\\EventManager\",\"type\":\"->\",\"args\":[{},{}]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zfcampus\\\\zf-rest\\\\src\\\\Resource.php\",\"line\":237,\"function\":\"triggerEvent\",\"class\":\"ZF\\\\Rest\\\\Resource\",\"type\":\"->\",\"args\":[\"create\",{\"data\":{\"resource_name\":\"assdfsfad\",\"resource_desc\":\"asdsadfsadfsadf\",\"owner\":\"sergio@trend2b.com\",\"id_type\":\"4\"}}]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zfcampus\\\\zf-rest\\\\src\\\\RestController.php\",\"line\":370,\"function\":\"create\",\"class\":\"ZF\\\\Rest\\\\Resource\",\"type\":\"->\",\"args\":[{\"resource_name\":\"assdfsfad\",\"resource_desc\":\"asdsadfsadfsadf\",\"owner\":\"sergio@trend2b.com\",\"id_type\":\"4\"}]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zfcampus\\\\zf-rest\\\\src\\\\RestController.php\",\"line\":889,\"function\":\"create\",\"class\":\"ZF\\\\Rest\\\\RestController\",\"type\":\"->\",\"args\":[{\"resource_name\":\"assdfsfad\",\"resource_desc\":\"asdsadfsadfsadf\",\"owner\":\"sergio@trend2b.com\",\"id_type\":\"4\"}]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zendframework\\\\zend-mvc\\\\src\\\\Controller\\\\AbstractRestfulController.php\",\"line\":416,\"function\":\"processPostData\",\"class\":\"ZF\\\\Rest\\\\RestController\",\"type\":\"->\",\"args\":[{}]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zfcampus\\\\zf-rest\\\\src\\\\RestController.php\",\"line\":333,\"function\":\"onDispatch\",\"class\":\"Zend\\\\Mvc\\\\Controller\\\\AbstractRestfulController\",\"type\":\"->\",\"args\":[{}]},{\"function\":\"onDispatch\",\"class\":\"ZF\\\\Rest\\\\RestController\",\"type\":\"->\",\"args\":[{}]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zendframework\\\\zend-eventmanager\\\\src\\\\EventManager.php\",\"line\":444,\"function\":\"call_user_func\",\"args\":[[{},\"onDispatch\"],{}]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zendframework\\\\zend-eventmanager\\\\src\\\\EventManager.php\",\"line\":205,\"function\":\"triggerListeners\",\"class\":\"Zend\\\\EventManager\\\\EventManager\",\"type\":\"->\",\"args\":[\"dispatch\",{},{}]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zendframework\\\\zend-mvc\\\\src\\\\Controller\\\\AbstractController.php\",\"line\":118,\"function\":\"trigger\",\"class\":\"Zend\\\\EventManager\\\\EventManager\",\"type\":\"->\",\"args\":[\"dispatch\",{},{}]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zendframework\\\\zend-mvc\\\\src\\\\Controller\\\\AbstractRestfulController.php\",\"line\":300,\"function\":\"dispatch\",\"class\":\"Zend\\\\Mvc\\\\Controller\\\\AbstractController\",\"type\":\"->\",\"args\":[{},{}]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zendframework\\\\zend-mvc\\\\src\\\\DispatchListener.php\",\"line\":93,\"function\":\"dispatch\",\"class\":\"Zend\\\\Mvc\\\\Controller\\\\AbstractRestfulController\",\"type\":\"->\",\"args\":[{},{}]},{\"function\":\"onDispatch\",\"class\":\"Zend\\\\Mvc\\\\DispatchListener\",\"type\":\"->\",\"args\":[{}]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zendframework\\\\zend-eventmanager\\\\src\\\\EventManager.php\",\"line\":444,\"function\":\"call_user_func\",\"args\":[[{},\"onDispatch\"],{}]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zendframework\\\\zend-eventmanager\\\\src\\\\EventManager.php\",\"line\":205,\"function\":\"triggerListeners\",\"class\":\"Zend\\\\EventManager\\\\EventManager\",\"type\":\"->\",\"args\":[\"dispatch\",{},{}]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\vendor\\\\zendframework\\\\zend-mvc\\\\src\\\\Application.php\",\"line\":314,\"function\":\"trigger\",\"class\":\"Zend\\\\EventManager\\\\EventManager\",\"type\":\"->\",\"args\":[\"dispatch\",{},{}]},{\"file\":\"C:\\\\xampp\\\\htdocs\\\\philips-api\\\\public\\\\index.php\",\"line\":51,\"function\":\"run\",\"class\":\"Zend\\\\Mvc\\\\Application\",\"type\":\"->\",\"args\":[]}]}],\"type\":\"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\"title\":\"Internal Server Error\",\"status\":500,\"detail\":\"Statement could not be executed (HY000 - 1364 - Field \'teste\' doesn\'t have a default value)\"},\"status\":500,\"config\":{\"method\":\"POST\",\"transformRequest\":[null],\"transformResponse\":[null],\"url\":\"http://philips-api.local/resources\",\"data\":{\"resource_name\":\"assdfsfad\",\"resource_desc\":\"asdsadfsadfsadf\",\"owner\":\"sergio@trend2b.com\",\"id_type\":\"4\"},\"headers\":{\"Accept\":\"application/json, text/plain, */*\",\"Content-Type\":\"application/json;charset=utf-8\",\"Authorization\":\"Bearer 1678bf6943670fd139f9b1145de55a9882575b8d\"}},\"statusText\":\"Internal Server Error\"}'),(11,'sergio@trend2b.com','O usuário inseriu uma nova permissão','resources','2016-01-26 10:42:24',3,'{\"data\":{\"id_resource\":\"37\",\"resource_name\":\"jhsdfgsd\",\"resource_desc\":\"jhdsjkdsfkdsfhkgsdkjfhgsdk\",\"created_on\":\"2016-01-26 10:42:21\",\"updated_on\":\"2016-01-26 10:42:21\",\"owner\":\"sergio@trend2b.com\",\"id_type\":\"3\",\"active\":\"1\",\"_links\":{\"self\":{\"href\":\"http://philips-api.local/resources/37\"}}},\"status\":201,\"config\":{\"method\":\"POST\",\"transformRequest\":[null],\"transformResponse\":[null],\"url\":\"http://philips-api.local/resources\",\"data\":{\"resource_name\":\"jhsdfgsd\",\"resource_desc\":\"jhdsjkdsfkdsfhkgsdkjfhgsdk\",\"owner\":\"sergio@trend2b.com\",\"id_type\":\"3\"},\"headers\":{\"Accept\":\"application/json, text/plain, */*\",\"Content-Type\":\"application/json;charset=utf-8\",\"Authorization\":\"Bearer 1678bf6943670fd139f9b1145de55a9882575b8d\"}},\"statusText\":\"Created\"}'),(12,'sergio@trend2b.com','O usuário alterou a permissão 1','resources','2016-01-26 12:31:21',4,'{\"data\":{\"id_resource\":\"1\",\"resource_name\":\"Botão Pagar\",\"resource_desc\":\"Permite que o usuário pague a meta na aba XPTO\",\"created_on\":\"2016-01-12 17:00:07\",\"updated_on\":\"2016-01-26 12:31:15\",\"owner\":\"sergio@trend2b.com\",\"id_type\":\"1\",\"active\":\"0\",\"_links\":{\"self\":{\"href\":\"http://philips-api.local/resources/1\"}}},\"status\":200,\"config\":{\"method\":\"PATCH\",\"transformRequest\":[null],\"transformResponse\":[null],\"url\":\"http://philips-api.local/resources/1\",\"data\":{\"active\":0},\"headers\":{\"Accept\":\"application/json, text/plain, */*\",\"Content-Type\":\"application/json;charset=utf-8\",\"Authorization\":\"Bearer ce6c4284caa2360e038dcacc6ee08e594f081b79\"}},\"statusText\":\"OK\"}'),(13,'sergio@trend2b.com','O usuário alterou a permissão 1','resources','2016-01-26 12:32:56',4,'{\"data\":{\"id_resource\":\"1\",\"resource_name\":\"Botão Pagar\",\"resource_desc\":\"Permite que o usuário pague a meta na aba XPTO\",\"created_on\":\"2016-01-12 17:00:07\",\"updated_on\":\"2016-01-26 12:32:54\",\"owner\":\"sergio@trend2b.com\",\"id_type\":\"1\",\"active\":\"1\",\"_links\":{\"self\":{\"href\":\"http://philips-api.local/resources/1\"}}},\"status\":200,\"config\":{\"method\":\"PATCH\",\"transformRequest\":[null],\"transformResponse\":[null],\"url\":\"http://philips-api.local/resources/1\",\"data\":{\"active\":1},\"headers\":{\"Accept\":\"application/json, text/plain, */*\",\"Content-Type\":\"application/json;charset=utf-8\",\"Authorization\":\"Bearer ce6c4284caa2360e038dcacc6ee08e594f081b79\"}},\"statusText\":\"OK\"}'),(14,'sergio@trend2b.com','O usuário alterou a permissão 1','resources','2016-01-26 12:35:05',4,'{\"data\":{\"id_resource\":\"1\",\"resource_name\":\"Botão Pagar\",\"resource_desc\":\"Permite que o usuário pague a meta na aba XPTO\",\"created_on\":\"2016-01-12 17:00:07\",\"updated_on\":\"2016-01-26 12:35:02\",\"owner\":\"sergio@trend2b.com\",\"id_type\":\"1\",\"active\":\"0\",\"_links\":{\"self\":{\"href\":\"http://philips-api.local/resources/1\"}}},\"status\":200,\"config\":{\"method\":\"PATCH\",\"transformRequest\":[null],\"transformResponse\":[null],\"url\":\"http://philips-api.local/resources/1\",\"data\":{\"active\":0},\"headers\":{\"Accept\":\"application/json, text/plain, */*\",\"Content-Type\":\"application/json;charset=utf-8\",\"Authorization\":\"Bearer ce6c4284caa2360e038dcacc6ee08e594f081b79\"}},\"statusText\":\"OK\"}'),(15,'sergio@trend2b.com','O usuário alterou a permissão 1','resources','2016-01-26 12:35:39',4,'{\"data\":{\"id_resource\":\"1\",\"resource_name\":\"Botão Pagar\",\"resource_desc\":\"Permite que o usuário pague a meta na aba XPTO\",\"created_on\":\"2016-01-12 17:00:07\",\"updated_on\":\"2016-01-26 12:35:37\",\"owner\":\"sergio@trend2b.com\",\"id_type\":\"1\",\"active\":\"1\",\"_links\":{\"self\":{\"href\":\"http://philips-api.local/resources/1\"}}},\"status\":200,\"config\":{\"method\":\"PATCH\",\"transformRequest\":[null],\"transformResponse\":[null],\"url\":\"http://philips-api.local/resources/1\",\"data\":{\"active\":1},\"headers\":{\"Accept\":\"application/json, text/plain, */*\",\"Content-Type\":\"application/json;charset=utf-8\",\"Authorization\":\"Bearer ce6c4284caa2360e038dcacc6ee08e594f081b79\"}},\"statusText\":\"OK\"}'),(16,'sergio@trend2b.com','O usuário alterou a permissão 1','resources','2016-01-26 12:59:46',4,'{\"data\":{\"id_resource\":\"1\",\"resource_name\":\"Botão Pagar\",\"resource_desc\":\"Permite que o usuário pague a meta na aba XPTO\",\"created_on\":\"2016-01-12 17:00:07\",\"updated_on\":\"2016-01-26 12:59:43\",\"owner\":\"sergio@trend2b.com\",\"id_type\":\"1\",\"active\":\"0\",\"_links\":{\"self\":{\"href\":\"http://philips-api.local/resources/1\"}}},\"status\":200,\"config\":{\"method\":\"PATCH\",\"transformRequest\":[null],\"transformResponse\":[null],\"url\":\"http://philips-api.local/resources/1\",\"data\":{\"active\":0},\"headers\":{\"Accept\":\"application/json, text/plain, */*\",\"Content-Type\":\"application/json;charset=utf-8\",\"Authorization\":\"Bearer ce6c4284caa2360e038dcacc6ee08e594f081b79\"}},\"statusText\":\"OK\"}'),(17,'sergio@trend2b.com','O usuário alterou a permissão 1','resources','2016-01-26 13:00:25',4,'{\"data\":{\"id_resource\":\"1\",\"resource_name\":\"Botão Pagar\",\"resource_desc\":\"Permite que o usuário pague a meta na aba XPTO\",\"created_on\":\"2016-01-12 17:00:07\",\"updated_on\":\"2016-01-26 13:00:22\",\"owner\":\"sergio@trend2b.com\",\"id_type\":\"1\",\"active\":\"1\",\"_links\":{\"self\":{\"href\":\"http://philips-api.local/resources/1\"}}},\"status\":200,\"config\":{\"method\":\"PATCH\",\"transformRequest\":[null],\"transformResponse\":[null],\"url\":\"http://philips-api.local/resources/1\",\"data\":{\"active\":1},\"headers\":{\"Accept\":\"application/json, text/plain, */*\",\"Content-Type\":\"application/json;charset=utf-8\",\"Authorization\":\"Bearer ce6c4284caa2360e038dcacc6ee08e594f081b79\"}},\"statusText\":\"OK\"}'),(18,'sergio@trend2b.com','O usuário alterou a permissão 1','resources','2016-01-26 13:07:26',4,'{\"data\":{\"id_resource\":\"1\",\"resource_name\":\"Botão Pagar\",\"resource_desc\":\"Permite que o usuário pague a meta na aba XPTO\",\"created_on\":\"2016-01-12 17:00:07\",\"updated_on\":\"2016-01-26 13:07:16\",\"owner\":\"sergio@trend2b.com\",\"id_type\":\"1\",\"active\":\"0\",\"_links\":{\"self\":{\"href\":\"http://philips-api.local/resources/1\"}}},\"status\":200,\"config\":{\"method\":\"PATCH\",\"transformRequest\":[null],\"transformResponse\":[null],\"url\":\"http://philips-api.local/resources/1\",\"data\":{\"active\":0},\"headers\":{\"Accept\":\"application/json, text/plain, */*\",\"Content-Type\":\"application/json;charset=utf-8\",\"Authorization\":\"Bearer ce6c4284caa2360e038dcacc6ee08e594f081b79\"}},\"statusText\":\"OK\"}'),(19,'sergio@trend2b.com','O usuário alterou a permissão 1','resources','2016-01-26 13:10:26',4,'{\"data\":{\"id_resource\":\"1\",\"resource_name\":\"Botão Pagar\",\"resource_desc\":\"Permite que o usuário pague a meta na aba XPTO\",\"created_on\":\"2016-01-12 17:00:07\",\"updated_on\":\"2016-01-26 13:10:23\",\"owner\":\"sergio@trend2b.com\",\"id_type\":\"1\",\"active\":\"1\",\"_links\":{\"self\":{\"href\":\"http://philips-api.local/resources/1\"}}},\"status\":200,\"config\":{\"method\":\"PATCH\",\"transformRequest\":[null],\"transformResponse\":[null],\"url\":\"http://philips-api.local/resources/1\",\"data\":{\"active\":1},\"headers\":{\"Accept\":\"application/json, text/plain, */*\",\"Content-Type\":\"application/json;charset=utf-8\",\"Authorization\":\"Bearer ce6c4284caa2360e038dcacc6ee08e594f081b79\"}},\"statusText\":\"OK\"}'),(20,'sergio@trend2b.com','O usuário alterou a permissão 1','resources','2016-01-26 13:14:45',4,'{\"data\":{\"id_resource\":\"1\",\"resource_name\":\"Botão Pagar\",\"resource_desc\":\"Permite que o usuário pague a meta na aba XPTO\",\"created_on\":\"2016-01-12 17:00:07\",\"updated_on\":\"2016-01-26 13:14:41\",\"owner\":\"sergio@trend2b.com\",\"id_type\":\"1\",\"active\":\"0\",\"_links\":{\"self\":{\"href\":\"http://philips-api.local/resources/1\"}}},\"status\":200,\"config\":{\"method\":\"PATCH\",\"transformRequest\":[null],\"transformResponse\":[null],\"url\":\"http://philips-api.local/resources/1\",\"data\":{\"active\":0},\"headers\":{\"Accept\":\"application/json, text/plain, */*\",\"Content-Type\":\"application/json;charset=utf-8\",\"Authorization\":\"Bearer ce6c4284caa2360e038dcacc6ee08e594f081b79\"}},\"statusText\":\"OK\"}'),(21,'sergio@trend2b.com','O usuário alterou a permissão 21','resources','2016-01-26 13:46:17',4,'{\"data\":{\"id_resource\":\"21\",\"resource_name\":\"157784\",\"resource_desc\":\"4658464646\",\"created_on\":\"2016-01-20 09:41:23\",\"updated_on\":\"2016-01-26 13:46:14\",\"owner\":\"pegaDoCookie@trend2.com\",\"id_type\":\"4\",\"active\":\"0\",\"_links\":{\"self\":{\"href\":\"http://philips-api.local/resources/21\"}}},\"status\":200,\"config\":{\"method\":\"PATCH\",\"transformRequest\":[null],\"transformResponse\":[null],\"url\":\"http://philips-api.local/resources/21\",\"data\":{\"active\":0},\"headers\":{\"Accept\":\"application/json, text/plain, */*\",\"Content-Type\":\"application/json;charset=utf-8\",\"Authorization\":\"Bearer d205cdf533237b509a3178b80272d2d9804cc95b\"}},\"statusText\":\"OK\"}'),(22,'sergio@trend2b.com','O usuário alterou a permissão 31','resources','2016-01-26 13:51:53',4,'{\"data\":{\"id_resource\":\"31\",\"resource_name\":\"4563122312312313123\",\"resource_desc\":\"3122132133246549879\",\"created_on\":\"2016-01-26 09:55:46\",\"updated_on\":\"2016-01-26 13:51:51\",\"owner\":\"sergio@trend2b.com\",\"id_type\":\"3\",\"active\":\"0\",\"_links\":{\"self\":{\"href\":\"http://philips-api.local/resources/31\"}}},\"status\":200,\"config\":{\"method\":\"PATCH\",\"transformRequest\":[null],\"transformResponse\":[null],\"url\":\"http://philips-api.local/resources/31\",\"data\":{\"active\":0},\"headers\":{\"Accept\":\"application/json, text/plain, */*\",\"Content-Type\":\"application/json;charset=utf-8\",\"Authorization\":\"Bearer d205cdf533237b509a3178b80272d2d9804cc95b\"}},\"statusText\":\"OK\"}'),(23,'augusto@trend2b.com','Ocorreu um erro ao alterar a permissão','resources','2016-01-26 18:16:27',2,'{\"data\":{\"validation_messages\":{\"resource_name\":{\"isEmpty\":\"Value is required and can\'t be empty\"},\"resource_desc\":{\"isEmpty\":\"Value is required and can\'t be empty\"},\"owner\":{\"isEmpty\":\"Value is required and can\'t be empty\"},\"id_type\":{\"isEmpty\":\"Value is required and can\'t be empty\"}},\"type\":\"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\"title\":\"Unprocessable Entity\",\"status\":422,\"detail\":\"Failed Validation\"},\"status\":422,\"config\":{\"method\":\"PATCH\",\"transformRequest\":[null],\"transformResponse\":[null],\"url\":\"http://philips-api.local/resources/14\",\"headers\":{\"Accept\":\"application/json, text/plain, */*\",\"Authorization\":\"Bearer 9a6aeceb34bae901bc3caf8f4cf620688f78531e\"}},\"statusText\":\"Unprocessable Entity\"}'),(24,'augusto@trend2b.com','Ocorreu um erro ao alterar a permissão','resources','2016-01-27 09:10:21',2,'{\"data\":{\"validation_messages\":{\"resource_name\":{\"isEmpty\":\"Value is required and can\'t be empty\"},\"resource_desc\":{\"isEmpty\":\"Value is required and can\'t be empty\"},\"owner\":{\"isEmpty\":\"Value is required and can\'t be empty\"},\"id_type\":{\"isEmpty\":\"Value is required and can\'t be empty\"}},\"type\":\"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html\",\"title\":\"Unprocessable Entity\",\"status\":422,\"detail\":\"Failed Validation\"},\"status\":422,\"config\":{\"method\":\"PATCH\",\"transformRequest\":[null],\"transformResponse\":[null],\"url\":\"http://philips-api.local/resources/11\",\"headers\":{\"Accept\":\"application/json, text/plain, */*\",\"Authorization\":\"Bearer 99df9fc829c004cefbbdb397daf2a5901eb61ac1\"}},\"statusText\":\"Unprocessable Entity\"}'),(25,'augusto@trend2b.com','O usuário alterou uma permissão','resources','2016-01-27 09:13:38',4,'{\"data\":{\"id_resource\":\"11\",\"resource_name\":\"Teste Toaster\",\"resource_desc\":\"Teste de toaster padrãozao zaooo\",\"created_on\":\"2016-01-19 18:48:06\",\"updated_on\":\"2016-01-27 09:13:35\",\"owner\":\"augusto@trend2b.com\",\"id_type\":\"4\",\"active\":\"1\",\"_links\":{\"self\":{\"href\":\"http://philips-api.local/resources/11\"}}},\"status\":200,\"config\":{\"method\":\"PATCH\",\"transformRequest\":[null],\"transformResponse\":[null],\"url\":\"http://philips-api.local/resources/11\",\"data\":{\"resource_desc\":\"Teste de toaster padrãozao zaooo\",\"owner\":\"augusto@trend2b.com\",\"id_type\":\"4\"},\"headers\":{\"Accept\":\"application/json, text/plain, */*\",\"Content-Type\":\"application/json;charset=utf-8\",\"Authorization\":\"Bearer dadc72bd67976a2e2b6a87e6961362390280ec4b\"}},\"statusText\":\"OK\"}'),(26,'augusto@trend2b.com','O usuário alterou a permissão 13','resources','2016-01-27 10:37:06',4,'{\"data\":{\"id_resource\":\"13\",\"resource_name\":\"teeeeete\",\"resource_desc\":\"Permite que o usuário pague a meta na aba ZETZA\",\"created_on\":\"2016-01-19 18:59:09\",\"updated_on\":\"2016-01-27 10:37:04\",\"owner\":\"augusto@trend2b.com\",\"id_type\":\"2\",\"active\":\"0\",\"_links\":{\"self\":{\"href\":\"http://philips-api.local/resources/13\"}}},\"status\":200,\"config\":{\"method\":\"PATCH\",\"transformRequest\":[null],\"transformResponse\":[null],\"url\":\"http://philips-api.local/resources/13\",\"data\":{\"active\":0},\"headers\":{\"Accept\":\"application/json, text/plain, */*\",\"Content-Type\":\"application/json;charset=utf-8\",\"Authorization\":\"Bearer 7f697380272a0b1c770b1e2c7ec8c7b8826b18f0\"}},\"statusText\":\"OK\"}'),(27,'augusto@trend2b.com','O usuário alterou uma permissão','resources','2016-01-27 10:49:00',4,'{\"data\":{\"id_resource\":\"13\",\"resource_name\":\"teeeeete\",\"resource_desc\":\"Permite que o usuário pague a meta na aba METSO\",\"created_on\":\"2016-01-19 18:59:09\",\"updated_on\":\"2016-01-27 10:48:57\",\"owner\":\"augusto@trend2b.com\",\"id_type\":\"2\",\"active\":\"0\",\"_links\":{\"self\":{\"href\":\"http://philips-api.local/resources/13\"}}},\"status\":200,\"config\":{\"method\":\"PATCH\",\"transformRequest\":[null],\"transformResponse\":[null],\"url\":\"http://philips-api.local/resources/13\",\"data\":{\"resource_desc\":\"Permite que o usuário pague a meta na aba METSO\",\"owner\":\"augusto@trend2b.com\",\"id_type\":\"2\"},\"headers\":{\"Accept\":\"application/json, text/plain, */*\",\"Content-Type\":\"application/json;charset=utf-8\",\"Authorization\":\"Bearer 72fe23b93c9ab950774ad704d34fe2e32576b7c8\"}},\"statusText\":\"OK\"}'),(28,'augusto@trend2b.com','O usuário alterou a permissão 20','resources','2016-01-27 11:27:45',4,'{\"data\":{\"id_resource\":\"20\",\"resource_name\":\"hjdjslpoopopop\",\"resource_desc\":\"popop\",\"created_on\":\"2016-01-20 09:40:27\",\"updated_on\":\"2016-01-27 11:27:43\",\"owner\":\"pegaDoCookie@trend2.com\",\"id_type\":\"2\",\"active\":\"0\",\"_links\":{\"self\":{\"href\":\"http://philips-api.local/resources/20\"}}},\"status\":200,\"config\":{\"method\":\"PATCH\",\"transformRequest\":[null],\"transformResponse\":[null],\"url\":\"http://philips-api.local/resources/20\",\"data\":{\"active\":0},\"headers\":{\"Accept\":\"application/json, text/plain, */*\",\"Content-Type\":\"application/json;charset=utf-8\",\"Authorization\":\"Bearer 72fe23b93c9ab950774ad704d34fe2e32576b7c8\"}},\"statusText\":\"OK\"}'),(29,'augusto@trend2b.com','O usuário alterou a permissão 27','resources','2016-01-27 11:44:38',4,'{\"data\":{\"id_resource\":\"27\",\"resource_name\":\"hjdghsdjk\",\"resource_desc\":\"sadfsdafsadfasdf\",\"created_on\":\"2016-01-26 09:42:07\",\"updated_on\":\"2016-01-27 11:44:35\",\"owner\":\"sergio@trend2b.com\",\"id_type\":\"4\",\"active\":\"0\",\"_links\":{\"self\":{\"href\":\"http://philips-api.local/resources/27\"}}},\"status\":200,\"config\":{\"method\":\"PATCH\",\"transformRequest\":[null],\"transformResponse\":[null],\"url\":\"http://philips-api.local/resources/27\",\"data\":{\"active\":0},\"headers\":{\"Accept\":\"application/json, text/plain, */*\",\"Content-Type\":\"application/json;charset=utf-8\",\"Authorization\":\"Bearer 296371b5eb18c0d8903d07f8d2f73ca02ef50bb5\"}},\"statusText\":\"OK\"}');
/*!40000 ALTER TABLE `log_application` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_types`
--

DROP TABLE IF EXISTS `log_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_types` (
  `id_log_type` int(10) NOT NULL,
  `type` varchar(45) COLLATE utf8_bin NOT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_log_type`),
  UNIQUE KEY `type_UNIQUE` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_types`
--

LOCK TABLES `log_types` WRITE;
/*!40000 ALTER TABLE `log_types` DISABLE KEYS */;
INSERT INTO `log_types` VALUES (1,'Login','2016-01-22 17:47:31','2016-01-22 19:47:31'),(2,'Error','2016-01-22 17:47:31','2016-01-22 19:47:31'),(3,'Create','2016-01-22 17:47:32','2016-01-22 19:47:32'),(4,'Update','2016-01-22 17:47:32','2016-01-22 19:47:32'),(5,'Read','2016-01-22 17:47:33','2016-01-22 19:47:33');
/*!40000 ALTER TABLE `log_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_access_tokens` (
  `access_token` varchar(40) NOT NULL,
  `client_id` varchar(80) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`access_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_tokens`
--

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `oauth_access_tokens` VALUES ('025a46bf523db56ef854c1041be3ede4d5fe8736','webApp','augusto@trend2b.com','2016-01-13 21:47:20',NULL),('02fd6e295d03d4d66b552dee4db6c8207575cf63','webApp','augusto@trend2b.com','2016-01-12 23:05:04',NULL),('04b36be22f0ea3e91c81635728db1eb9a50f4585','webApp','sergio@trend2b.com','2016-01-26 20:45:13',NULL),('04f203c126341f85ca60ad66dc9cf0cd1a69d5ec','webApp','augusto@trend2b.com','2016-01-15 14:57:09',NULL),('05bf727cf05833af8a54ce6d534d0742bce54460','webApp','sergio@trend2b.com','2016-01-22 23:13:55',NULL),('06240cfd87d99aef15ab01e03d9a8be5662998bc','webApp','tony@trend2b.com','2016-01-20 21:27:30',NULL),('079a2cf8ca848616614507c83ed838ac4b913bd1','webApp','augusto@trend2b.com','2016-01-13 15:19:23',NULL),('08e621a25574cd389df0957b0bca513b7c88271c','webApp','augusto@trend2b.com','2016-01-14 18:39:11',NULL),('08fc6b5684bf2eb80f08c6f71f7dc09a8233ed39','webApp','augusto@trend2b.com','2016-01-13 21:44:01',NULL),('09ef1524ec94b21fa2fc77cd73a6b693f2c5c926','webApp','augusto@trend2b.com','2016-01-19 23:12:27',NULL),('0b17d5e13abd50b146ea1943e7b1fe1ac890cfd2','webApp','augusto@trend2b.com','2016-01-13 16:41:09',NULL),('0b1e5934262adb996c75c7e72cf8900c9ed904e2','webApp','tony@trend2b.com','2016-01-22 23:27:21',NULL),('0c266b9d0acba2df1f619f18cd3317d093a845bc','webApp','sergio@trend2b.com','2016-01-28 23:30:01',NULL),('0e3eb3f9fb41852b3805d8c922f11b7822f3b33f','webApp','augusto@trend2b.com','2016-01-14 15:01:37',NULL),('0efa4c259f1e7d600778ada7ce14b3e99f568c8a','webApp','sergio@trend2b.com','2016-01-29 15:41:45',NULL),('104636da9de54b8687a9c2a02867ac3b87dd28e8','webApp','augusto@trend2b.com','2016-01-13 17:00:46',NULL),('112eda6d84e967e7c23bec3d24c4d2d2dbd6312e','webApp','augusto@trend2b.com','2016-01-04 19:37:40',NULL),('11595a0d1285b4e4febb95884e3fad4b18ff7d5d','webApp','augusto@trend2b.com','2016-01-13 21:20:53',NULL),('12853b7df73690886282a2fbc0749a96c5de41d8','webApp','augusto@trend2b.com','2016-01-18 17:55:45',NULL),('1437b4bde3fdf2f971d3c3656a565412e08654a5','webApp','augusto@trend2b.com','2016-01-20 20:50:01',NULL),('1678bf6943670fd139f9b1145de55a9882575b8d','webApp','sergio@trend2b.com','2016-01-26 16:35:14',NULL),('168ee0a63b48c1adab2613b611d1f751c66b6787','webApp','sergio@trend2b.com','2016-01-26 15:28:20',NULL),('1711c12c88f1a5c97ff05fb4c7dc3ffdda7a865f','webApp','augusto@trend2b.com','2016-01-19 19:50:48',NULL),('17f264091966ea7d516fa1be021c031ff600beca','webApp','augusto@trend2b.com','2016-01-13 21:28:49',NULL),('198f3e0a35ed87ada51c020a3909bbc3d8a2adbc','webApp','sergio@trend2b.com','2016-01-26 23:14:02',NULL),('19964a19d764ef363e61f3abb6c8057bdd7c2cff','webApp','augusto@trend2b.com','2016-01-13 22:35:36',NULL),('1acbea1e0827e51d246700b01d3d078c570d0150','webApp','augusto@trend2b.com','2016-01-13 21:14:44',NULL),('1c52428d86f1fa2fcb5dc5467606a3a9d6143b14','webApp','augusto@trend2b.com','2016-01-20 18:39:21',NULL),('1d3310175bd3bcd42479c818ccb39a4ad7266103','webApp','tony@trend2b.com','2016-01-20 21:07:48',NULL),('1f6ae0f3af57a00b1bf5244822acf3b0cf31c103','webApp','sergio@trend2b.com','2016-01-26 23:31:31',NULL),('2017caf430e2b4cdd63a4389cbea4ce399d536a9','webApp','augusto@trend2b.com','2016-01-15 14:57:31',NULL),('2087e46ba008ca6ff863ecc48696528ab1f903f8','webApp','augusto@trend2b.com','2016-01-13 21:50:18',NULL),('2163482da361cb2a3583cbda242e59d086c30df1','webApp','augusto@trend2b.com','2016-01-13 15:48:58',NULL),('2230cce771fb3d468248b5a5092f8392a5e43b07','webApp','augusto@trend2b.com','2016-01-14 22:32:05',NULL),('228b403f48a49e70932c68246cdb7d50c6cd2dd2','webApp','augusto@trend2b.com','2016-01-05 17:43:12',NULL),('2374c9aee39807b8b4bae03739a85b67ec8adfd5','webApp','augusto@trend2b.com','2016-01-13 15:15:51',NULL),('23d5803546b81ffa9738dcd6f98612742c6bea80','webApp','tony@trend2b.com','2016-01-20 21:14:27',NULL),('241fc6321035cad6444108d8a46696886adaa810','webApp','augusto@trend2b.com','2016-01-04 19:33:33',NULL),('274bda64ec5cc0c807d97be0ca26b81a104ebaae','webApp','augusto@trend2b.com','2016-01-14 20:52:28',NULL),('2859059042e24129d829c33f8f21a6f2d360f336','webApp','augusto@trend2b.com','2016-01-13 21:20:15',NULL),('28aaf5cdf73d76d4f6e43aca7f497e6e71d63b83','webApp','augusto@trend2b.com','2016-01-13 21:03:44',NULL),('296371b5eb18c0d8903d07f8d2f73ca02ef50bb5','webApp','augusto@trend2b.com','2016-01-27 17:41:03',NULL),('2add16aaafc22dbc1762dbfd1d8c69468e8d06fb','webApp','tony@trend2b.com','2016-01-26 15:39:53',NULL),('2d26a5db0e1468b14289dfedcd5f95091beae9e2','webApp','augusto@trend2b.com','2016-01-04 19:38:00',NULL),('2d3b671c033d75ca466b8c86fffc2f55e7541910','webApp','augusto@trend2b.com','2016-01-08 14:57:05',NULL),('2f72586bef67456a65fedc7245376bae7cceeddc','webApp','augusto@trend2b.com','2016-01-13 13:33:27',NULL),('30e57ab30607665b5bfa8e04db2f9c111373828a','webApp','augusto@trend2b.com','2016-01-13 21:29:33',NULL),('32f2a1f2a46246cb8fe6165080aae39ece514af6','webApp','sergio@trend2b.com','2016-01-28 22:12:41',NULL),('33179b733140293fcb384390f791617f1f7c6a2e','webApp','augusto@trend2b.com','2016-01-20 15:09:38',NULL),('339ccf6d52caf07623d546f3232a1389ac49e149','webApp','augusto@trend2b.com','2016-01-13 22:30:44',NULL),('33af3d7c3f4429a58100164db5f3fee783706bcb','webApp','sergio@trend2b.com','2016-01-29 16:47:43',NULL),('34556375f31398fc37ffe6c44781c636f625bbb4','webApp','tony@trend2b.com','2016-01-20 21:26:19',NULL),('346bde8581a5fe431a22f6adb916f764fc281b82','webApp','sergio@trend2b.com','2016-01-28 19:45:28',NULL),('350b7d0aed525d6a0950d1c3a22cc07766347433','webApp','augusto@trend2b.com','2016-01-19 21:13:43',NULL),('3614ffeda13ae885ee45ef2e63f64d4cb5ee7c06','webApp','augusto@trend2b.com','2016-01-13 15:50:14',NULL),('367934bdb64bcd8912c66db01661aa1e7fdda21c','webApp','augusto@trend2b.com','2016-01-19 17:14:04',NULL),('3763b798f6d31c379c2668f449cbd0ff2ce74614','webApp','augusto@trend2b.com','2016-01-13 16:01:04',NULL),('3773242eddcfe7bb8088481ee7a54add27d7f8f0','webApp','sergio@trend2b.com','2016-01-26 19:28:10',NULL),('37e00baaf69975cd332c4acadd9f96a08d93a045','webApp','sergio@trend2b.com','2016-01-28 20:45:10',NULL),('37f6f9010b475c328efff35563b88f3999ba2be7','webApp','augusto@trend2b.com','2016-01-05 18:04:36',NULL),('384bb993671b4ea22a46773af5c5d8b068453c5a','webApp','augusto@trend2b.com','2016-01-13 22:30:14',NULL),('394f8f1b03a928e72da89d9e4967e75edd664953','webApp','sergio@trend2b.com','2016-01-26 19:30:14',NULL),('39b47bb47c98b694a649f2b3c48c9cde60dd17b7','webApp','tony@trend2b.com','2016-01-20 21:13:39',NULL),('3b1b096efef2c074401bb9e2ed5aaad1df5f016c','webApp','augusto@trend2b.com','2016-01-12 22:41:27',NULL),('3b66e9bf7072950dc6fcda4465a44a4e74f56bc2','webApp','augusto@trend2b.com','2016-01-13 21:34:35',NULL),('3c391ec83aa4bf2b62e7a9a52fa766ad870ed840','webApp','augusto@trend2b.com','2016-01-13 21:21:22',NULL),('3d13d87ae8ed1c87cc54c86e26fd2589059409f6','webApp','augusto@trend2b.com','2016-01-18 23:40:09',NULL),('3d610764fc464d0603f2d40dfdb7efb1d60a16a5','webApp','augusto@trend2b.com','2016-01-13 21:42:59',NULL),('3fcbc34f3fd7346bb973b5ee4ac94cf6aeea0df9','webApp','augusto@trend2b.com','2016-01-20 21:14:58',NULL),('3fe358171cf0b3c9e1eee04c4ed04861cbc8492c','webApp','augusto@trend2b.com','2016-01-12 23:27:23',NULL),('402b048216aa8eaaa8355c3ad4982fa0fde1bda1','webApp','augusto@trend2b.com','2016-01-13 19:39:28',NULL),('41a87537ee3afc8f6d5949b554a61205963b090e','webApp','augusto@trend2b.com','2016-01-27 22:27:20',NULL),('420ddeef9e1f874a8771a8960f0085a3e983571b','webApp','augusto@trend2b.com','2016-01-20 15:24:18',NULL),('43343c448b8a64844b300e78ffb6434a69c32dc0','webApp','augusto@trend2b.com','2016-01-05 12:28:51',NULL),('43f88b4e462d694349300a8742c9b2b9e0a69a28','webApp','augusto@trend2b.com','2016-01-20 00:42:59',NULL),('44eb635885826b6059642ec2abe573660fc268b5','webApp','sergio@trend2b.com','2016-01-28 21:11:03',NULL),('4651a574de4f8736569f06248c093848b3c21166','webApp','augusto@trend2b.com','2016-01-13 14:44:04',NULL),('4662f04db01fcb1bfe2c63d73a928dd1c2914728','webApp','augusto@trend2b.com','2016-01-13 22:13:45',NULL),('4855cdebb339b8103a2e5de7a004edf291ad1d4b','webApp','augusto@trend2b.com','2016-01-28 16:05:00',NULL),('497846b0c269573848067b8fe8fe906da790f0ce','webApp','augusto@trend2b.com','2016-01-14 22:12:37',NULL),('4a83cf46b553be0aa3bbf80ec5cbe590e74fdd22','webApp','sergio@trend2b.com','2016-01-26 23:02:55',NULL),('4e4d48bf01c384da3ef81eed7b2e64e17011a65e','webApp','augusto@trend2b.com','2016-01-13 21:39:55',NULL),('4fced68ee14d2ed088c76fb974067646cf0e60ed','webApp','augusto@trend2b.com','2016-01-27 19:00:56',NULL),('507e431fd6c6a573757a57ee28e44444e37e8a05','webApp','sergio@trend2b.com','2016-01-26 19:37:36',NULL),('53099796a3e2b95971bceb7696be7bdd99cd43dd','webApp','augusto@trend2b.com','2016-01-05 21:50:14',NULL),('54eeaf70b9421610229f03d81db9beef579648cc','webApp','augusto@trend2b.com','2016-01-13 21:45:54',NULL),('555e05220b50d0cf838d6f67163711338dd17677','webApp','augusto@trend2b.com','2016-01-13 20:11:22',NULL),('55a0fa6f0bfafce9b5394c2df3d1b9f3a5e8df33','webApp','sergio@trend2b.com','2016-01-28 16:50:50',NULL),('55a9e4eea1733f33fa0486784b04bcb4c3355029','webApp','sergio@trend2b.com','2016-01-26 19:31:58',NULL),('56901a4301867f9a1ef8147e3a3a90d7b072d6e9','webApp','augusto@trend2b.com','2016-01-14 23:42:29',NULL),('575f96ac1f04d7e199d0bc598ce1cf174ca7519b','webApp','augusto@trend2b.com','2016-01-13 21:20:02',NULL),('57e6e28c07909948b7e5ee05530adae2d2a3abe2','webApp','augusto@trend2b.com','2016-01-20 20:51:14',NULL),('58c38d256cb4c99bbc612d5b059c5ce3ae164f87','webApp','sergio@trend2b.com','2016-01-26 19:30:39',NULL),('5917bd3e32447ea9174d9313a401b1e6da0443d5','webApp','sergio@trend2b.com','2016-01-26 17:23:11',NULL),('5be4373c75e2caacd98eaba7e15913deecc7892e','webApp','augusto@trend2b.com','2016-01-20 20:00:52',NULL),('5d8c2f441a0366332cda81712c0f15f220354409','webApp','augusto@trend2b.com','2016-01-04 19:34:59',NULL),('5df5c0131a8e5584af564ffbd8ff4b011209c3b5','webApp','sergio@trend2b.com','2016-01-26 19:29:17',NULL),('5ef5f7ebae1ba8743a452c5927da1ab8e8547310','webApp','augusto@trend2b.com','2016-01-12 14:49:39',NULL),('5f9b976de9d7c5de37a6a7aaa5a92f7e53ef774a','webApp','augusto@trend2b.com','2016-01-12 22:22:41',NULL),('613370ddfc54e439ae82f1e93ecd11375f321d09','webApp','tony@trend2b.com','2016-01-20 21:08:48',NULL),('629a315c62172cc0319f6486a5e6b4e429f187d4','webApp','sergio@trend2b.com','2016-01-26 16:32:20',NULL),('62f4748767f784c9786fc9c32f61e97baa96673c','webApp','augusto@trend2b.com','2016-01-04 19:53:52',NULL),('64b3e794887db8ac766af3dfd09331f37aff4fd4','webApp','sergio@trend2b.com','2016-01-26 23:10:13',NULL),('6713acccb970840d80dd300558b047c26beff547','webApp','augusto@trend2b.com','2016-01-13 15:58:10',NULL),('6bb28b37dfb787ff837d023a372df254f6e611a8','webApp','sergio@trend2b.com','2016-01-29 15:41:20',NULL),('6e04b1632f49c7b5f73cdbd5213145e4807fb2dc','webApp','augusto@trend2b.com','2016-01-05 10:53:19',NULL),('6fea0f4bdc973ee8044a874f6d75ae8585e3b811','webApp','sergio@trend2b.com','2016-01-22 23:19:24',NULL),('7144b49ba5f7def130e264e8def2a2500b9574a8','webApp','sergio@trend2b.com','2016-01-28 19:09:21',NULL),('72095c3b2fc4879cae24164e341c43e0aa5de472','webApp','augusto@trend2b.com','2016-01-18 16:10:31',NULL),('72b264c5c6a91c37615e93af83efb648a3af7ae2','webApp','sergio@trend2b.com','2016-01-27 23:02:50',NULL),('72c81b2cba5105793110c5758778576bbfb1b7ab','webApp','augusto@trend2b.com','2016-01-13 17:02:18',NULL),('72fe23b93c9ab950774ad704d34fe2e32576b7c8','webApp','augusto@trend2b.com','2016-01-27 16:40:26',NULL),('7629fa3d2a743deb5b3169f4a4ef68b2b9a25577','webApp','augusto@trend2b.com','2016-01-04 19:33:13',NULL),('77bc8faa64b77fd92433fcf13c9c57b2af633c93','webApp','sergio@trend2b.com','2016-01-26 21:49:35',NULL),('78e5c3202f74f373ccdf32f59b21ac9e1cd51cde','webApp','augusto@trend2b.com','2016-01-13 21:03:24',NULL),('7d46cbfd8ed533a37a7140eb31fbd2824754e752','webApp','augusto@trend2b.com','2016-01-20 21:05:48',NULL),('7deb13aed8f9c4b198efa8b4aff6547429572af5','webApp','augusto@trend2b.com','2016-01-05 18:08:33',NULL),('7f583a5031a4e5ea18a68beb7b435409861f3f00','webApp','augusto@trend2b.com','2016-01-13 20:03:02',NULL),('7f697380272a0b1c770b1e2c7ec8c7b8826b18f0','webApp','augusto@trend2b.com','2016-01-27 16:28:09',NULL),('815af2be53ab453460a731a2e11c091a6df617a0','webApp','augusto@trend2b.com','2016-01-05 18:40:50',NULL),('8b296b29a33fdd3cc76c69fd29961300a92e171a','webApp','augusto@trend2b.com','2016-01-20 21:00:55',NULL),('8c2bfaa9b2c485263ed44c7f4ffe5bd0f40e099d','webApp','augusto@trend2b.com','2016-01-13 15:33:18',NULL),('910290a76f3c41d254d3fce8add6b973e55a9ddc','webApp','sergio@trend2b.com','2016-01-29 14:27:57',NULL),('9133b0fff421b77adff0f6e483fbba67d67f14e6','webApp','augusto@trend2b.com','2016-01-20 20:50:30',NULL),('921c2bc3b6569896a3e4ce304af4baf183c45386','webApp','augusto@trend2b.com','2016-01-13 22:17:49',NULL),('9269f0074d97af6f73232c67cd5eb1e179b4a267','webApp','sergio@trend2b.com','2016-01-28 20:09:55',NULL),('9275a9bd69d59ecfd68f702b1e1a5626793a2291','webApp','augusto@trend2b.com','2016-01-26 19:33:23',NULL),('9516793890f425e8aa861bf1c69fad35292afc35','webApp','sergio@trend2b.com','2016-01-28 22:27:38',NULL),('96790b43eef1a29aff247146cc7b6b01aed58ff2','webApp','augusto@trend2b.com','2016-01-04 19:35:21',NULL),('96bec641ad517328ffdc2707fad24c72442666d8','webApp','augusto@trend2b.com','2016-01-20 20:48:23',NULL),('96c364cd4876d641bb846a81410379e47d57919c','webApp','augusto@trend2b.com','2016-01-13 21:07:24',NULL),('99df9fc829c004cefbbdb397daf2a5901eb61ac1','webApp','augusto@trend2b.com','2016-01-27 14:17:10',NULL),('9a6aeceb34bae901bc3caf8f4cf620688f78531e','webApp','augusto@trend2b.com','2016-01-26 23:24:07',NULL),('9bcae836e23893a2ff70d74669c1b5d31ed3fa1a','webApp','augusto@trend2b.com','2016-01-13 21:46:46',NULL),('a090162eeae7ff747ebdf10089d5508e47e4c8b1','webApp','augusto@trend2b.com','2016-01-13 15:06:29',NULL),('a1348d6e0d311d29595bfb758ae91f12299a11ad','webApp','augusto@trend2b.com','2016-01-20 00:17:52',NULL),('a3420fbbeda9942b016ad040ca06ed30002d7de1','webApp','augusto@trend2b.com','2016-01-13 22:29:02',NULL),('a3c9d959b3d77cc6c280cf564fc60b09abf7fb37','webApp','augusto@trend2b.com','2016-01-20 21:03:11',NULL),('a48fe0923c734fd2223d02970de9edaa4a3c4df2','webApp','augusto@trend2b.com','2016-01-13 21:19:23',NULL),('a600745bf0be4e86de4863cf8bc1cac3e321233b','webApp','augusto@trend2b.com','2016-01-14 23:33:25',NULL),('a61e5dae648410279f2bb705a8262372938339d3','webApp','augusto@trend2b.com','2016-01-20 17:39:01',NULL),('a6ea2cbfc660853ae57412dad1bb94e945594cb2','webApp','augusto@trend2b.com','2016-01-18 23:36:00',NULL),('aa8005de0eca71ae0477cee1fbc0b802d8e8136e','webApp','augusto@trend2b.com','2016-01-13 21:08:59',NULL),('acaadd2970bee600915dd8d7e65feb7ab0f0b9af','webApp','augusto@trend2b.com','2016-01-14 16:04:16',NULL),('aecda1a530c5c2bf64e6b4be90e7975b7fcf7dec','webApp','sergio@trend2b.com','2016-01-26 21:55:55',NULL),('af5e751242d39e5b9fb9810312b229ef619d5623','webApp','sergio@trend2b.com','2016-01-28 23:59:12',NULL),('afd783c8dcf36efdc216016d543fe94610e8e415','webApp','augusto@trend2b.com','2016-01-04 19:22:40',NULL),('b25cf0b5ee733d71c5860c563ca01d7c8321c7f1','webApp','augusto@trend2b.com','2016-01-14 14:53:19',NULL),('b2f959dddd45e906614defa6c124191abfa627e2','webApp','augusto@trend2b.com','2016-01-13 14:01:06',NULL),('b36a11fd9d9797d3814d9b412afcd22ca897d7f3','webApp','augusto@trend2b.com','2016-01-13 15:57:33',NULL),('b4d19a27184d3b89ee91726e1d1a637d462bf50c','webApp','tony@trend2b.com','2016-01-26 16:57:07',NULL),('b69588bd618dcdf05a6b6618e77be62c7584e079','webApp','augusto@trend2b.com','2016-01-05 21:47:39',NULL),('b8e39c9ead0e2ea0125edeecdc52e81043eb7f35','webApp','augusto@trend2b.com','2016-01-28 23:22:41',NULL),('b8f33f13873ea915f72f3161f86c1a2e03cd12ce','webApp','sergio@trend2b.com','2016-01-28 18:08:07',NULL),('b9365b6281ca057e01589b8cb96f4eee57abfc9c','webApp','augusto@trend2b.com','2016-01-13 15:52:25',NULL),('b953391de36219ea995b30529a81b025369eff83','webApp','augusto@trend2b.com','2016-01-13 21:33:49',NULL),('b9f6597c19f27f5b9e21aaf6690b4cc679d1c51f','webApp','augusto@trend2b.com','2016-01-20 21:06:34',NULL),('bb6f16fbae50b48eae9608048c040535aab3366a','webApp','augusto@trend2b.com','2016-01-04 20:29:47',NULL),('bc33bea3861350f331ae6eb7b98e8cc09f8960d1','webApp','augusto@trend2b.com','2016-01-27 23:18:59',NULL),('bd1ea65c1d3039ccea9b6427ee7b2c3ec7703f43','webApp','augusto@trend2b.com','2016-01-13 15:14:45',NULL),('bd2029c91060e92e4f87eb0b27aacb96aa088ba4','webApp','augusto@trend2b.com','2016-01-13 21:22:31',NULL),('bebff1f96e0b835cccb6cf748344441c904ab320','webApp','augusto@trend2b.com','2016-01-13 21:42:27',NULL),('bec78b855521f9699169059a866ebcd018040444','webApp','augusto@trend2b.com','2016-01-20 21:06:05',NULL),('bffc31bc89047c7b062407171a0439d006429325','webApp','tony@trend2b.com','2016-01-26 19:31:48',NULL),('c0740509b8f9fa93c2fd0b60903da2afbc0eebf3','webApp','sergio@trend2b.com','2016-01-22 22:54:42',NULL),('c1cb980b799f189b056b93374c2e2b92b57ed842','webApp','augusto@trend2b.com','2016-01-13 15:58:49',NULL),('c26236b8acee07288816dab0fc8ccdc94a122380','webApp','augusto@trend2b.com','2016-01-13 21:15:39',NULL),('c82dd99f69e6e05f14419e7c6c1772d9f8b990a1','webApp','sergio@trend2b.com','2016-01-26 19:28:52',NULL),('c8ddd57eda27bd96bbb905c18af27d045af0239d','webApp','augusto@trend2b.com','2016-01-05 18:42:22',NULL),('ca1a32da732369142ea23be260241a2e77b2425d','webApp','augusto@trend2b.com','2016-01-19 18:22:00',NULL),('cbf7a0f84401d542eeb180623da1469c95a196ba','webApp','sergio@trend2b.com','2016-01-26 19:34:05',NULL),('ce6c4284caa2360e038dcacc6ee08e594f081b79','webApp','sergio@trend2b.com','2016-01-26 18:26:10',NULL),('cef9812dfdef95d18443aadc38d5c99d233c643e','webApp','augusto@trend2b.com','2016-01-12 22:01:21',NULL),('cf6de6fb47694e009c2a4d66442f38f7d317a6df','webApp','augusto@trend2b.com','2016-01-14 23:48:51',NULL),('d03cdb0d3beedc14f08ad829f8b0abd1c56dfe5f','webApp','augusto@trend2b.com','2016-01-12 22:46:24',NULL),('d205cdf533237b509a3178b80272d2d9804cc95b','webApp','sergio@trend2b.com','2016-01-26 19:43:58',NULL),('d2cb90297fbcb4942de391236ffa8193be4e7e95','webApp','augusto@trend2b.com','2016-01-13 19:54:24',NULL),('d4e5e5485de562b2e9dd20a0cafe8e288d6b5a29','webApp','augusto@trend2b.com','2016-01-19 19:51:49',NULL),('d585595c94c78a24ab402b713f6068c2677bc94e','webApp','augusto@trend2b.com','2016-01-13 21:02:44',NULL),('d6a32738a9467689e8862edb5e353eaf9a3974a2','webApp','sergio@trend2b.com','2016-01-20 21:35:38',NULL),('d980e4e976f9e65bd06c59259fbe99d259651249','webApp','augusto@trend2b.com','2016-01-13 21:14:10',NULL),('dadc72bd67976a2e2b6a87e6961362390280ec4b','webApp','augusto@trend2b.com','2016-01-27 15:12:56',NULL),('dd6e6be6105118b7901d6fe7c1c244686f058fa9','webApp','augusto@trend2b.com','2016-01-19 22:14:04',NULL),('de2757cc6e8013e19863a5787213910a35aaaa5b','webApp','augusto@trend2b.com','2016-01-13 15:55:39',NULL),('de6fabaa641e4f270d74b82aca12edba9b231af4','webApp','augusto@trend2b.com','2016-01-13 16:10:51',NULL),('e003fdc0dd3ec8eb4375710d47670c35b0de48c7','webApp','augusto@trend2b.com','2016-01-04 19:32:36',NULL),('e03f51b6bfafd93c44648e71e3fd6e7aeaa71f1c','webApp','augusto@trend2b.com','2016-01-13 21:05:15',NULL),('e1e01d83d97130d8a11386d68f770c88fa5dad65','webApp','augusto@trend2b.com','2016-01-04 19:39:54',NULL),('e34e94547ee9152848fae5e4ae04bbe6fc973fc9','webApp','augusto@trend2b.com','2016-01-13 22:31:44',NULL),('e5adbcf078cd72fb1c50d545bcf84c34229d71d3','webApp','tony@trend2b.com','2016-01-20 21:08:19',NULL),('e5b0bc1cbfa6bd6017e023c93364a7c46737f181','webApp','tony@trend2b.com','2016-01-26 19:35:37',NULL),('e6256c36894362ef2f6983ead0cce13c54b96662','webApp','augusto@trend2b.com','2016-01-18 23:44:27',NULL),('e6a1016ad74068f4605b073d05be866b5505031b','webApp','augusto@trend2b.com','2016-01-19 18:20:37',NULL),('e6a351587bd0702b2b1e105f1cf18a63c609a029','webApp','augusto@trend2b.com','2016-01-13 15:58:36',NULL),('ea77a701d38b61dc0aed2a6a67a39f9db0c85503','webApp','sergio@trend2b.com','2016-01-20 21:31:26',NULL),('eaa8f0d1762c59b3d2f505ba684c471e804c1590','webApp','augusto@trend2b.com','2016-01-20 20:47:22',NULL),('eb8d92f0a1116cbe9e5dc3e392b80a5f9fc3fef3','webApp','augusto@trend2b.com','2016-01-14 22:32:25',NULL),('ecd88b0cb5359df4b5a2497774bdd24f1977e67d','webApp','augusto@trend2b.com','2016-01-13 21:15:27',NULL),('ee43d1422bd9f08436d349bcd87946e1beac4c72','webApp','augusto@trend2b.com','2016-01-19 19:40:52',NULL),('f16d6a2fb88824be52feb67e616db3f1706eb3e2','webApp','augusto@trend2b.com','2016-01-13 16:19:44',NULL),('f22f17603ea694cbd8eaf5828f259decd734829e','webApp','augusto@trend2b.com','2016-01-13 15:18:01',NULL),('f44bf99a53e9edf16901faa654ddeeee516bca0f','webApp','sergio@trend2b.com','2016-01-26 19:35:55',NULL),('f66bd5025c77a7cc9ceb6871d450c34b726eb3c1','webApp','augusto@trend2b.com','2016-01-05 18:05:13',NULL),('f742fdc95782e258a0f375d11cf1f76ef44bd542','webApp','sergio@trend2b.com','2016-01-26 22:58:10',NULL),('f7911327c83072f7064d00b484aeee56cc427d87','webApp','augusto@trend2b.com','2016-01-14 21:10:40',NULL),('f99279bc1c31f03549cf1f99ac061eafda502b09','webApp','augusto@trend2b.com','2016-01-08 14:47:13',NULL),('fb073de6f3da1f2ed71f69950e645a183ae415a0','webApp','tony@trend2b.com','2016-01-26 18:06:29',NULL),('fbc0e4aa1e385639547867538511a003180f0126','webApp','sergio@trend2b.com','2016-01-26 19:36:58',NULL),('fbcf8019b15243fb4d3988691570949d2f9308c3','webApp','augusto@trend2b.com','2016-01-13 16:37:32',NULL),('fe6ba080e2bce92f36e7a95c822658459e51671d','webApp','augusto@trend2b.com','2016-01-13 21:44:47',NULL);
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_authorization_codes`
--

DROP TABLE IF EXISTS `oauth_authorization_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_authorization_codes` (
  `authorization_code` varchar(40) NOT NULL,
  `client_id` varchar(80) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `redirect_uri` varchar(2000) DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(2000) DEFAULT NULL,
  `id_token` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`authorization_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_authorization_codes`
--

LOCK TABLES `oauth_authorization_codes` WRITE;
/*!40000 ALTER TABLE `oauth_authorization_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_authorization_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_clients` (
  `client_id` varchar(80) NOT NULL,
  `client_secret` varchar(80) NOT NULL,
  `redirect_uri` varchar(2000) NOT NULL,
  `grant_types` varchar(80) DEFAULT NULL,
  `scope` varchar(2000) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_clients`
--

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` VALUES ('webApp','$2y$10$Fa190HBqvuHPAah80ejLUeM4MvJ//NoIZd7E6DCN1rY6KpoEaoCMG','',NULL,NULL,NULL);
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_jwt`
--

DROP TABLE IF EXISTS `oauth_jwt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_jwt` (
  `client_id` varchar(80) NOT NULL,
  `subject` varchar(80) DEFAULT NULL,
  `public_key` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_jwt`
--

LOCK TABLES `oauth_jwt` WRITE;
/*!40000 ALTER TABLE `oauth_jwt` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_jwt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_refresh_tokens` (
  `refresh_token` varchar(40) NOT NULL,
  `client_id` varchar(80) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`refresh_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_tokens`
--

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
INSERT INTO `oauth_refresh_tokens` VALUES ('0028dc5d82af49a0c037700b69713880531db2ea','webApp','sergio@trend2b.com','2016-02-05 21:54:43',NULL),('0244f5e8e08f0ef787a306f44f113006c52107ed','webApp','augusto@trend2b.com','2016-01-18 19:16:59',NULL),('026ad8447b820a2fa5eb76b4263c7260c7270e81','webApp','augusto@trend2b.com','2016-01-19 17:50:33',NULL),('03406bf512c55d7eeb178be97dfbef905f7af949','webApp','augusto@trend2b.com','2016-01-26 14:31:39',NULL),('043666088aa3b2b53575a3d54513135fd6026a4c','webApp','augusto@trend2b.com','2016-01-19 21:32:15',NULL),('06e4ca048aa398cdb21472dc1fa9b53bf4dc21fd','webApp','sergio@trend2b.com','2016-02-09 18:34:06',NULL),('0724a682968f987b1d4d94b07f13f5c45227b070','webApp','augusto@trend2b.com','2016-01-28 14:01:37',NULL),('08c94cc2aaa6ec9dc834c8df7152c5bef9f2a668','webApp','augusto@trend2b.com','2016-02-03 19:00:53',NULL),('098be120fbe19513e91146cfe4a6bb1cc4c7bd03','webApp','augusto@trend2b.com','2016-02-03 20:05:48',NULL),('0b32f9985abeb3e5dc6b5098b332a07db56f54b8','webApp','sergio@trend2b.com','2016-02-09 22:02:59',NULL),('0d1dd45743b8936113c82e3e2dfbb5a7b0842ccd','webApp','augusto@trend2b.com','2016-01-19 18:24:22',NULL),('0e3b588b33273d32fb8ca6f007f168693f008698','webApp','augusto@trend2b.com','2016-01-27 20:34:36',NULL),('0e93975a2058128e25772b694ed5b316316e60be','webApp','sergio@trend2b.com','2016-02-09 18:31:59',NULL),('0f8771b67f940f5924e8523b35de71d3751daa13','webApp','tony@trend2b.com','2016-02-03 20:07:49',NULL),('1029ccce959c5ed6768e8d79b4ede16ed1d9c1e6','webApp','augusto@trend2b.com','2016-02-01 22:36:00',NULL),('102fc830c9517683e42a22837f00b148670b332f','webApp','augusto@trend2b.com','2016-01-27 20:21:22',NULL),('112f8d9ee0c6627ecb7243dee05d2e174b40ea7a','webApp','augusto@trend2b.com','2016-01-27 18:54:24',NULL),('11f7a71714a1b14d9c5bf583b8a717ac8a8efd8d','webApp','augusto@trend2b.com','2016-01-27 20:02:45',NULL),('128013d95d43ac97c8e0f7964c0d7410929ff04c','webApp','augusto@trend2b.com','2016-01-27 16:00:46',NULL),('130177677daf1e81c868474d5382d37831d735b0','webApp','augusto@trend2b.com','2016-01-27 20:50:18',NULL),('133e3ad17a37353767af25e7cf3a124a7ef89cb6','webApp','augusto@trend2b.com','2016-01-27 15:01:04',NULL),('14d46945d584937cfb43f2a2a3bb60eeae26bc8a','webApp','augusto@trend2b.com','2016-02-10 16:41:03',NULL),('160e0cda5e4bebe8d30795d86000ad3323472bc6','webApp','augusto@trend2b.com','2016-02-02 21:14:04',NULL),('179221d5bd178a7669d66d86ea4ec5ebf6de0ac0','webApp','sergio@trend2b.com','2016-02-09 18:30:15',NULL),('19fe3ca81850a67ff976100f5a49d72b64c7ccf7','webApp','sergio@trend2b.com','2016-02-11 17:08:07',NULL),('1a16a21e9438ca47cbe3b20033db0c14c81e07ec','webApp','sergio@trend2b.com','2016-02-03 20:35:38',NULL),('1a55047bc172557e876d50cca6258f769475471c','webApp','augusto@trend2b.com','2016-01-27 20:14:10',NULL),('1a8c9b835579b8a852c9fba5da157d204fa9dd3a','webApp','sergio@trend2b.com','2016-02-11 15:50:51',NULL),('1a8d48c77b70139b5a6482b15ae7fca16dd67381','webApp','augusto@trend2b.com','2016-01-27 21:29:02',NULL),('1ab4c2b8f1f4980c3e4d9dc83f13560c2b4285a9','webApp','sergio@trend2b.com','2016-02-11 19:09:55',NULL),('1d1765e8c81eb59a6feb04022b1c471157d7424c','webApp','augusto@trend2b.com','2016-01-27 16:02:19',NULL),('1d419b15cfd53ca7a6f9130f0e4fd12f3ad497cf','webApp','augusto@trend2b.com','2016-02-03 20:06:34',NULL),('1eb5436e24390123d7a31e2fc02c4ac20f19fbcc','webApp','augusto@trend2b.com','2016-02-01 22:44:27',NULL),('1ff321b7aee56da6aabedeaa03473d44eae7aa31','webApp','augusto@trend2b.com','2016-01-18 19:19:40',NULL),('2142083209cc349201265217382a64905c14eaee','webApp','augusto@trend2b.com','2016-01-19 17:46:36',NULL),('21c77ec5173851c75e452c892e7161a4580e38be','webApp','augusto@trend2b.com','2016-02-02 20:13:43',NULL),('2251510e69eff36153c249b1b666ffda2eaf579d','webApp','tony@trend2b.com','2016-02-05 22:27:21',NULL),('26a763e254b30303e4097912f3d3580ce6e22373','webApp','sergio@trend2b.com','2016-02-09 18:29:17',NULL),('277d1cc3b7afd634dd9a18ab0e042d5a02899174','webApp','augusto@trend2b.com','2016-02-10 13:17:11',NULL),('290509879998c8c5158a0cd747906c1c5486c340','webApp','sergio@trend2b.com','2016-02-10 22:02:50',NULL),('2c07d8cbbbc92e24529b19d488593639b4af0a3d','webApp','augusto@trend2b.com','2016-02-02 18:50:49',NULL),('2c3def4db04107aad27e3992dd8fae401403684c','webApp','sergio@trend2b.com','2016-02-11 21:12:41',NULL),('2d6c6215a328ff3711cb34edf5d0ff5f7f9828e7','webApp','augusto@trend2b.com','2016-02-03 20:00:55',NULL),('2e75d762b018e412c28c604a35d1955a2a04e2e8','webApp','augusto@trend2b.com','2016-01-19 10:35:19',NULL),('311474840b1b11fb71e6cacbc336a04fb3bedf27','webApp','augusto@trend2b.com','2016-01-27 21:30:45',NULL),('3219f66c32a459114f71036d3260360dbed5df28','webApp','augusto@trend2b.com','2016-02-02 18:51:49',NULL),('33acfab21f575b82a37a73a8f54320c45d396e4e','webApp','augusto@trend2b.com','2016-02-03 20:06:06',NULL),('3585e4813fe4365f45c77a953c0bda00e95e4f4c','webApp','augusto@trend2b.com','2016-02-09 22:24:08',NULL),('3691fe837d13d393ecee04882f3c9bf349f889ea','webApp','sergio@trend2b.com','2016-02-09 18:35:55',NULL),('37a9f913d9fbaf18ee04177712c0bbe02155ce41','webApp','augusto@trend2b.com','2016-01-18 19:04:41',NULL),('387987e50b7202c24f729539f2ffb0dd892f733c','webApp','augusto@trend2b.com','2016-01-27 14:50:14',NULL),('390fa69af6bc9565d6a7be3106578a993e114d13','webApp','augusto@trend2b.com','2016-01-27 20:09:00',NULL),('39272e7e846a371aa2bf841881aa5442ec521380','webApp','augusto@trend2b.com','2016-01-27 20:19:23',NULL),('3987052aaf2259e1f4914bdf886ceb7b516b74fa','webApp','augusto@trend2b.com','2016-01-27 13:15:27',NULL),('3b20faf00ce615b99e9e51bd43c1aa25c2fb6a9c','webApp','augusto@trend2b.com','2016-01-27 15:19:44',NULL),('3d5be294dc6ae3642d7e79c15bcef86b5031d22f','webApp','augusto@trend2b.com','2016-01-22 14:39:06',NULL),('41d5592f8ef63d05449c0acc1d8a9f57ccac046c','webApp','sergio@trend2b.com','2016-02-09 14:28:21',NULL),('423890c9c0b6083581a418a070eff4bd4666e797','webApp','augusto@trend2b.com','2016-02-10 15:28:09',NULL),('42d92c180007d9cb6867e6cc4183a07e305c660a','webApp','augusto@trend2b.com','2016-01-28 17:39:11',NULL),('433c81bde4f0647d3c9d82c00247cf6cb404c6f7','webApp','augusto@trend2b.com','2016-01-18 19:15:13',NULL),('434e517036005d9b7ec93e30f6f997d93671945a','webApp','augusto@trend2b.com','2016-01-27 19:11:23',NULL),('4782650740b82405e7c4efbe40293a381fb3f5d2','webApp','augusto@trend2b.com','2016-01-27 21:30:14',NULL),('489551df4fcf4835513c9ba59837b328628afd8c','webApp','sergio@trend2b.com','2016-02-11 19:45:11',NULL),('49012ea2c439eb6d52bc7183721d62bee5bfab29','webApp','sergio@trend2b.com','2016-02-09 20:55:56',NULL),('495f998569ba2506959e21657f895f8562d8b5c3','webApp','sergio@trend2b.com','2016-02-09 18:28:52',NULL),('4989b217d9f5761cd62aefa59510fcf144656212','webApp','augusto@trend2b.com','2016-01-28 20:10:40',NULL),('4a18d1cc89636ea5e50fef54ee639edf11b53914','webApp','augusto@trend2b.com','2016-02-11 15:05:00',NULL),('4d9bb3b5f09aa66af89310add716697915d0ad2d','webApp','augusto@trend2b.com','2016-01-28 22:33:26',NULL),('4ebc6dc70ff87c19f7183fddf7b98e8c0fb429ad','webApp','augusto@trend2b.com','2016-02-02 17:20:38',NULL),('509aedf2ab7c7f5e71fdd4e446062b9b678c2cb2','webApp','augusto@trend2b.com','2016-02-03 16:39:01',NULL),('514da0c5aebeb6ea9a3022ef8b35483a2fb74d86','webApp','augusto@trend2b.com','2016-01-26 21:43:21',NULL),('522a833669d5f2f58f27d143d9484101a1f6538f','webApp','sergio@trend2b.com','2016-02-12 15:47:43',NULL),('524cecdb01ba3749d2e3d4032f587857d1992eeb','webApp','augusto@trend2b.com','2016-01-18 19:15:33',NULL),('534870b265423e1984dabbf31bccec81d03d6d46','webApp','augusto@trend2b.com','2016-01-27 20:20:15',NULL),('541ddac46c25198f4f697a54b28aab0898d0f913','webApp','augusto@trend2b.com','2016-01-28 19:52:28',NULL),('551a1c78c14a6be277f038063d879ffef0e891a4','webApp','augusto@trend2b.com','2016-01-22 14:29:14',NULL),('5cd9ca44c70f558a7f1b66128dac907425556a98','webApp','augusto@trend2b.com','2016-01-27 18:39:29',NULL),('5cf1a24b4d3f2dc0facd53d4308d28254dcb83a0','webApp','augusto@trend2b.com','2016-01-27 21:35:37',NULL),('5e62f72f5168dff0b8e3864a2dddba009ba6d4f6','webApp','sergio@trend2b.com','2016-02-09 18:37:37',NULL),('5f00cd67e68a259aa73230215a14013f7e60b998','webApp','augusto@trend2b.com','2016-01-27 20:44:02',NULL),('5ff929840f2482762620f723aab5737ae19f3762','webApp','sergio@trend2b.com','2016-02-12 14:41:45',NULL),('60a59b1d829778cd2234ba1af60796defd9ef05b','webApp','augusto@trend2b.com','2016-01-27 13:44:05',NULL),('60c68b3cb77685251487662c5dbc00c9541bebf2','webApp','tony@trend2b.com','2016-02-09 18:31:48',NULL),('62b634a8ef27c517758de2073d081b92791a6fbc','webApp','augusto@trend2b.com','2016-01-27 14:18:01',NULL),('63508545af162fe24f83b5bb8a6dec3bd06296a9','webApp','augusto@trend2b.com','2016-02-02 16:14:05',NULL),('6371423992790ac124fe8ee5ab17f4d7d09691b0','webApp','sergio@trend2b.com','2016-02-09 18:36:58',NULL),('663b6983a6438a49497b1c8833e1b7b5c5f68f9b','webApp','augusto@trend2b.com','2016-01-27 21:17:49',NULL),('66e4ce16ff24b549b5440290cdcb30db2ad109d1','webApp','augusto@trend2b.com','2016-01-27 20:07:24',NULL),('6887a2f7eebec36bc36a9f09cde4dec7814ce8df','webApp','augusto@trend2b.com','2016-01-27 14:52:26',NULL),('68dba0a9c3dac6fce6c63f1d23babe8fa7d30a2f','webApp','sergio@trend2b.com','2016-02-03 20:31:26',NULL),('690dba493d1bd9db4fbf43edc7d43becb164fc1d','webApp','augusto@trend2b.com','2016-01-29 13:57:31',NULL),('6b4756068d8589f08a828191652726c7ea60b0ea','webApp','augusto@trend2b.com','2016-02-03 19:47:23',NULL),('6c0fb729a0a47dee6cb75913dd05be80236a8974','webApp','augusto@trend2b.com','2016-02-03 20:14:58',NULL),('6e2521f39aa8d4c909e70b3e2edacaac23f81970','webApp','tony@trend2b.com','2016-02-09 14:39:53',NULL),('6e2f8edfbc5e24b9c649bb8ef2cef8680f476691','webApp','augusto@trend2b.com','2016-02-01 15:10:32',NULL),('6e53428ec2a66510dc0e2ef0ffb81a9102c6fce2','webApp','augusto@trend2b.com','2016-01-28 15:04:16',NULL),('6e67783ad0157a8bed6e0664bf2f4f4f42a46f60','webApp','augusto@trend2b.com','2016-01-18 19:14:36',NULL),('70dfafea54e73b0c6f6d6e29c92dadd3fe028019','webApp','augusto@trend2b.com','2016-02-10 21:27:20',NULL),('725943291ea2614c8a28256c00708295366e7416','webApp','sergio@trend2b.com','2016-02-11 22:30:01',NULL),('72a792bc15b80d835b470f3b3ba2da90fe31b926','webApp','tony@trend2b.com','2016-02-09 18:35:37',NULL),('74f63461a3ccfe6798de7c8e3f5bad533bc8c773','webApp','tony@trend2b.com','2016-02-03 20:27:30',NULL),('77565dea7ad767e244895b100ea212a5dcc5bcf4','webApp','augusto@trend2b.com','2016-01-27 20:05:15',NULL),('78132ccc0b882e20b2712225f5f1f7b6da542226','webApp','augusto@trend2b.com','2016-02-02 18:40:52',NULL),('785d7ac7c113cadadaa5383530e1e2e25660d552','webApp','augusto@trend2b.com','2016-01-27 14:58:10',NULL),('788f102022aac11ea42eeb98ce090cfa7c055cc1','webApp','augusto@trend2b.com','2016-01-26 23:09:23',NULL),('7aa24c074b6f8af494ccd5669117c6b500d9fc5f','webApp','augusto@trend2b.com','2016-01-18 19:21:54',NULL),('7b8c01a41e7a030a1964d6326e55cc0a054d21aa','webApp','tony@trend2b.com','2016-02-09 15:57:08',NULL),('7c8782f0da01468ce7ebcbb938fc2f93d7ad863c','webApp','augusto@trend2b.com','2016-01-27 20:46:46',NULL),('7db874fb8cb25b55ebfefec3e0099882e781b037','webApp','sergio@trend2b.com','2016-02-11 22:59:13',NULL),('7df92290c944b7584617fc000f7816bb1c7adf3d','webApp','augusto@trend2b.com','2016-01-27 20:22:31',NULL),('7f2292350fe8cf67fc6803a686bc86e4f6610f13','webApp','augusto@trend2b.com','2016-02-02 23:42:59',NULL),('80c78c70f17aa7960dc6f5fd97bf7ae43f9f59b7','webApp','augusto@trend2b.com','2016-01-27 20:03:24',NULL),('816088ca5ce7b13993b9858688686029bc640025','webApp','sergio@trend2b.com','2016-02-09 19:45:14',NULL),('85291dc4ef905e3e5db26fab6eabc23572dfc4d4','webApp','augusto@trend2b.com','2016-01-19 17:47:13',NULL),('87f6b3b47b823292a5a87c173282840bcca3ff03','webApp','augusto@trend2b.com','2016-01-27 20:28:49',NULL),('888e42a8bb94ba6f911de6a5bf20cc71ca7d1eee','webApp','augusto@trend2b.com','2016-01-27 20:14:45',NULL),('89169e873e1fbf4635a818b066fb75d383f970ed','webApp','augusto@trend2b.com','2016-01-19 12:10:51',NULL),('892ac04b4853d3992cbfb6e7aeebab68868cf068','webApp','augusto@trend2b.com','2016-01-27 15:37:33',NULL),('8b1a0ee240057536d654e43af0abb9b2d57181c4','webApp','augusto@trend2b.com','2016-02-03 14:24:19',NULL),('8fc3f219ec8b4d5cb679666bcb2b3ad8ab034e9a','webApp','augusto@trend2b.com','2016-01-27 14:15:51',NULL),('904c28b65953f5a2c7f27c4774d6d213926802d3','webApp','augusto@trend2b.com','2016-02-02 17:22:01',NULL),('90ead96662d78227f8c02a3a4a8d5f958add2f85','webApp','augusto@trend2b.com','2016-01-27 19:03:03',NULL),('9322d893a803b365a2533602eeef22eaf8faa200','webApp','augusto@trend2b.com','2016-01-19 18:22:50',NULL),('9336533bbe0573e440614c514c3dea68a68b8c81','webApp','augusto@trend2b.com','2016-01-27 13:43:06',NULL),('93c5144d4825fdf0deb4b356642c2c6d8f434c7a','webApp','augusto@trend2b.com','2016-01-29 13:57:09',NULL),('93cbf4209344d761a113ad10f946411cbd4e29d3','webApp','augusto@trend2b.com','2016-01-27 14:06:29',NULL),('93ddbe559d7cb6d0701c1d4c9b6caf4e45ffc3cb','webApp','augusto@trend2b.com','2016-02-03 19:50:31',NULL),('9400bc3211e22379a075d8a3cb05ac20db37d1d6','webApp','tony@trend2b.com','2016-02-03 20:08:48',NULL),('9585e1b829c964a44c3f4fd184d27d8ce5904bd1','webApp','augusto@trend2b.com','2016-01-28 21:32:06',NULL),('966b1bac0225ab60b3186e38511ba56d6c66e8d5','webApp','augusto@trend2b.com','2016-01-27 20:42:27',NULL),('9711fb7435ec125ca4932e2a05f358e4c63b289d','webApp','sergio@trend2b.com','2016-02-09 21:58:10',NULL),('97800d2ba39fc6d67683621e20cb60df9f23ce66','webApp','augusto@trend2b.com','2016-01-27 20:44:47',NULL),('99540be758f20e297f7d18034cf9cbc3c86fe332','webApp','sergio@trend2b.com','2016-02-09 20:49:36',NULL),('99bf2cec53f92aab8549ed58ae96df4dcdab944a','webApp','augusto@trend2b.com','2016-01-27 20:45:54',NULL),('9bee109b2574894db50b3c7500b02a998dee0db0','webApp','augusto@trend2b.com','2016-02-03 19:50:01',NULL),('9bf0b9cdc1341e27aaf0c045d766a8e7a5bb2ea3','webApp','augusto@trend2b.com','2016-02-10 14:12:57',NULL),('9dd4c64f9aeb7ae33579d656f552ce46478b172d','webApp','sergio@trend2b.com','2016-02-09 16:23:12',NULL),('9e20f587b4be17d495bb8430501ed226ad599d8e','webApp','sergio@trend2b.com','2016-02-09 15:32:20',NULL),('9f0fa6326f6160dbe7403aac244be63e0e0633ab','webApp','augusto@trend2b.com','2016-02-10 18:00:56',NULL),('9f6714a298d9c2f56ab06d2cdd7e5a31a57a8f7c','webApp','augusto@trend2b.com','2016-01-27 20:39:56',NULL),('9fda0f9ce91e491e1eed7691a101ab8b0cfd6052','webApp','tony@trend2b.com','2016-02-03 20:08:19',NULL),('a11853c0c6143cb6727591a891e71fa0138341ce','webApp','augusto@trend2b.com','2016-01-18 20:11:47',NULL),('a2eb835f044a6d738af10e32e444a75b4962c684','webApp','augusto@trend2b.com','2016-01-26 22:28:24',NULL),('a35991d8861ec76a97fe0ba3ab7d256f808b2b8d','webApp','augusto@trend2b.com','2016-02-10 22:19:00',NULL),('a38b7da554d711446d17c7942dc12743956d0697','webApp','augusto@trend2b.com','2016-01-27 20:03:44',NULL),('a44f6d1b5d098ec0d9350d78a2ff5af8ca1f3a58','webApp','sergio@trend2b.com','2016-02-05 22:13:55',NULL),('a4bafef6c05ae752724b94f209c8bbef56d6f9f1','webApp','augusto@trend2b.com','2016-01-28 21:32:26',NULL),('a9025e62f5952d9b0cc56230c1962a25c6f4c2e5','webApp','augusto@trend2b.com','2016-02-03 19:48:24',NULL),('a973bda60838744e36d5cfb9779de284628b1fb6','webApp','sergio@trend2b.com','2016-02-09 22:31:31',NULL),('aaf9df22fa5b0d86b02689cd1ddc35a5a481c087','webApp','augusto@trend2b.com','2016-01-18 19:17:21',NULL),('ab36f0d6636b2016cb058b429060e1dcebde3feb','webApp','augusto@trend2b.com','2016-01-26 22:23:27',NULL),('ac1dac59321afe50aa1246c73f2c2cdbcc027c05','webApp','augusto@trend2b.com','2016-01-27 20:15:39',NULL),('ac5e113d53f9ff0d8fd9baa356fca89ffb596c75','webApp','sergio@trend2b.com','2016-02-09 22:14:03',NULL),('ad4579345a3c4c51fb629fcc8921378994347203','webApp','augusto@trend2b.com','2016-02-03 17:39:21',NULL),('adb5dcae85ef82af3b82b18d05b431fe828c19d2','webApp','augusto@trend2b.com','2016-02-03 14:09:38',NULL),('ade7e07cfd1753b4bc4231fd7ea8120ba123c61a','webApp','augusto@trend2b.com','2016-01-27 20:47:20',NULL),('b162be46eccc09c937c50fb6e8c3e74d42a1c75b','webApp','augusto@trend2b.com','2016-02-01 22:40:09',NULL),('b38071c3e9ce7c7267f8b55de9f34b3119f23486','webApp','augusto@trend2b.com','2016-02-02 23:17:52',NULL),('b5dbecc408383f0231e5c83b7601c3101a12f4d5','webApp','sergio@trend2b.com','2016-02-11 18:09:21',NULL),('b74ae3a94f498167dddd5b8b2b2d6a49bee99560','webApp','augusto@trend2b.com','2016-02-03 20:03:11',NULL),('b84a0e20a3f131cd76bdb640a66c43c95bc9b7db','webApp','augusto@trend2b.com','2016-01-27 20:29:33',NULL),('b9422f548d10e2edce0dc4637ac1488b643b056f','webApp','augusto@trend2b.com','2016-01-27 14:55:39',NULL),('bc668af218574314a7d7d20cc804bda1487daf02','webApp','augusto@trend2b.com','2016-01-19 21:29:40',NULL),('bd74d16119a84726b23c6184e13fd60aa0bd55d7','webApp','sergio@trend2b.com','2016-02-11 18:45:29',NULL),('befd81aeb821f040d4047cd56f1c55b5c7eaf7aa','webApp','augusto@trend2b.com','2016-01-27 14:57:33',NULL),('c0dcd8d1ee53aa52d4ae3d7372466640f6362505','webApp','sergio@trend2b.com','2016-02-09 18:43:59',NULL),('c1bcc7c053908a49df4d33292612a8fd68ea8b3c','webApp','tony@trend2b.com','2016-02-09 17:06:29',NULL),('c321e98b73aaedc8c47cfd3174f4d2faf6f126f2','webApp','augusto@trend2b.com','2016-01-27 14:33:18',NULL),('c33855ad9e5969241cf410d4e9fe332e3b7eda68','webApp','augusto@trend2b.com','2016-01-27 14:58:36',NULL),('c4d77b843ac3e44eab9776c5bfe6f6cbe2b9405f','webApp','augusto@trend2b.com','2016-02-02 22:12:28',NULL),('c4f025d095beba4c413bb1552ca318ad558cdd9c','webApp','augusto@trend2b.com','2016-02-03 19:51:14',NULL),('c510adecf58085904899142bb872fe7f0a21d950','webApp','augusto@trend2b.com','2016-01-27 21:13:45',NULL),('c68c44fbd3d9c0e479e1a06edea7b85154b9dd78','webApp','augusto@trend2b.com','2016-01-27 15:41:10',NULL),('c6fe66098b0cdea43d58f06cb8609b5677d3937d','webApp','augusto@trend2b.com','2016-01-27 20:20:02',NULL),('c8f2027202554cd08d36d234a2d1877b3f797069','webApp','augusto@trend2b.com','2016-01-27 20:33:50',NULL),('cc381e0ff9866a67bf68a00f8f5bf3478caaec68','webApp','sergio@trend2b.com','2016-02-09 18:28:10',NULL),('ccba76c5a315e729254268f81253dc05dbbcb2d2','webApp','augusto@trend2b.com','2016-01-27 15:10:52',NULL),('ce1166165a98e1bdcfbc37a5ac35590e519ae98e','webApp','sergio@trend2b.com','2016-02-11 20:11:04',NULL),('cf751460ec8b6fdeaa7f8cb0157812d2d7e15a7c','webApp','augusto@trend2b.com','2016-01-18 19:20:00',NULL),('d1c31ee03e196862e354142918397345417825e7','webApp','augusto@trend2b.com','2016-01-19 17:25:12',NULL),('d32eb0079abce0993abd05751812cd642b23e92f','webApp','augusto@trend2b.com','2016-01-28 22:48:51',NULL),('d36636a9752a9a0c5177af7c64bd674148593688','webApp','sergio@trend2b.com','2016-02-05 22:19:24',NULL),('d39d18d7edef32f77fe0685bff3cd07c33633752','webApp','augusto@trend2b.com','2016-01-28 22:42:30',NULL),('d3ea6ec6f5070991fe0ef2bd9d8c2ef8c67e9bda','webApp','augusto@trend2b.com','2016-01-27 14:19:24',NULL),('d6c134d52d63d9bb36f6500f4bb9fe808da733c2','webApp','sergio@trend2b.com','2016-02-09 17:26:11',NULL),('d6eb87a65f4c836407f04245c53bbfc7e0a4926e','webApp','augusto@trend2b.com','2016-01-18 19:35:52',NULL),('daf3b0fb3cc88b7ffcca489090120e8ca7390ba8','webApp','augusto@trend2b.com','2016-01-26 22:04:41',NULL),('db0a711fd501ccaf3a70557c814d15e5fc61cb77','webApp','augusto@trend2b.com','2016-02-01 16:55:46',NULL),('db0c9da28f919d74b65918031ead394dfc0140b3','webApp','sergio@trend2b.com','2016-02-11 21:27:39',NULL),('dc753c2460c0aab851a446cb95e82a468021d870','webApp','augusto@trend2b.com','2016-02-10 15:40:26',NULL),('dd66c3465ebb6baac0b751b36a5448d4333987ad','webApp','augusto@trend2b.com','2016-01-27 20:42:59',NULL),('ddacb29dfd6f2140ae10f5be4a5f6067645c44ac','webApp','tony@trend2b.com','2016-02-03 20:13:40',NULL),('de3c9464ab7adb7c7e8bb4f593bbc27cb33481d9','webApp','augusto@trend2b.com','2016-01-26 22:47:04',NULL),('df736441297f1bd23b088f8edbfd707887f5443a','webApp','tony@trend2b.com','2016-02-03 20:14:28',NULL),('dfe36bdf44ad82b267772de611eef9cab436b9a4','webApp','augusto@trend2b.com','2016-01-27 14:48:58',NULL),('e117aa0f8aa4acb262721fe964d99cf61e7925ab','webApp','augusto@trend2b.com','2016-02-09 18:33:24',NULL),('e3c3a680dc7b9f1fae0ad007670a93a5b4778525','webApp','sergio@trend2b.com','2016-02-09 22:10:14',NULL),('e52ac9b8591ba96f268b2351b53c67e0652eca84','webApp','augusto@trend2b.com','2016-01-27 21:31:44',NULL),('e68290d1fe9dc14410511fc99416563efd36a4ee','webApp','augusto@trend2b.com','2016-01-27 20:20:54',NULL),('e6997762816772f5e957127939e930e93efacd31','webApp','tony@trend2b.com','2016-02-03 20:26:19',NULL),('ea93cb68d4e22de57cd212ab37cffa04cb7f8835','webApp','sergio@trend2b.com','2016-02-09 15:35:14',NULL),('f0943973d21112b07a43b91f57f18452f828c771','webApp','augusto@trend2b.com','2016-02-11 22:22:41',NULL),('f0c2b5f7c82571621a86018703d37c7a6bdcef48','webApp','sergio@trend2b.com','2016-02-09 18:30:39',NULL),('f2fda1f5550c6ca6cbf22e73edd745c55a275fd7','webApp','sergio@trend2b.com','2016-02-12 14:41:20',NULL),('f3f831ef4e4de2bd59400069bf118c8faa91cb29','webApp','augusto@trend2b.com','2016-01-27 14:58:50',NULL),('f88ca3b5c0008a2673b19583b529ccc7b1370224','webApp','augusto@trend2b.com','2016-01-28 21:12:38',NULL),('f89bb7272dfc155bc26b57fae6ec09a031e55a00','webApp','augusto@trend2b.com','2016-01-27 14:14:46',NULL),('f9bde82c560b6ee61030f61f44ad1081b4eee0d5','webApp','augusto@trend2b.com','2016-01-28 13:53:19',NULL),('fc0438dab73fa64a7e3fc11119c1f1d8adf1fcc9','webApp','augusto@trend2b.com','2016-01-27 20:15:27',NULL),('fea3f9744d3fe681051bde284c841d1110fcf1b1','webApp','sergio@trend2b.com','2016-02-12 13:27:58',NULL);
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_scopes`
--

DROP TABLE IF EXISTS `oauth_scopes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_scopes` (
  `type` varchar(255) NOT NULL DEFAULT 'supported',
  `scope` varchar(2000) DEFAULT NULL,
  `client_id` varchar(80) DEFAULT NULL,
  `is_default` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_scopes`
--

LOCK TABLES `oauth_scopes` WRITE;
/*!40000 ALTER TABLE `oauth_scopes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_scopes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_users`
--

DROP TABLE IF EXISTS `oauth_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_users` (
  `id_user` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(2000) NOT NULL,
  `first_name` varchar(80) NOT NULL,
  `last_name` varchar(80) NOT NULL,
  `id_group` int(10) NOT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `owner` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `username` (`username`),
  KEY `fk_id_group_role_group` (`id_group`),
  CONSTRAINT `fk_id_group_role_group` FOREIGN KEY (`id_group`) REFERENCES `role_group` (`id_group`) ON DELETE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_users`
--

LOCK TABLES `oauth_users` WRITE;
/*!40000 ALTER TABLE `oauth_users` DISABLE KEYS */;
INSERT INTO `oauth_users` VALUES (1,'augusto@trend2b.com','$2y$10$FgkUO9RvN4i3RESFIyvPS.dhymsrP2Cw9.0YjTojpRxds4OdRDGH6','Augusto','Henriques',1,'2016-01-12 16:43:19','2016-01-12 18:43:19','sergio@trend2b.com',1),(2,'sergio@trend2b.com','$2y$10$FgkUO9RvN4i3RESFIyvPS.dhymsrP2Cw9.0YjTojpRxds4OdRDGH6','Sergio','Marchetti',2,'2016-01-20 10:34:39','2016-01-20 12:35:41','sergio@trend2b.com',1),(3,'tony@trend2b.com','$2y$10$FgkUO9RvN4i3RESFIyvPS.dhymsrP2Cw9.0YjTojpRxds4OdRDGH6','Tony','Cesar',2,'2016-01-20 10:35:06','2016-01-20 12:35:41','sergio@trend2b.com',1);
/*!40000 ALTER TABLE `oauth_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resources`
--

DROP TABLE IF EXISTS `resources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resources` (
  `id_resource` int(10) NOT NULL AUTO_INCREMENT,
  `resource_name` varchar(254) NOT NULL,
  `resource_desc` varchar(254) NOT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `owner` varchar(255) NOT NULL,
  `id_type` varchar(20) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_resource`),
  UNIQUE KEY `resourcename` (`resource_name`),
  KEY `fk_id_type_resources_type` (`id_type`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resources`
--

LOCK TABLES `resources` WRITE;
/*!40000 ALTER TABLE `resources` DISABLE KEYS */;
INSERT INTO `resources` VALUES (1,'Botão Pagar','Permite que o usuário pague a meta na aba XPTO','2016-01-12 17:00:07','2016-01-26 15:41:49','sergio@trend2b.com','1',1),(2,'Barra de Progresso Home Widget','Exibição da Barra de Progresso Home Page','2016-01-12 17:01:15','2016-01-13 13:48:39','sergio@trend2b.com','2',1),(3,'Coluna Number','Exibição da Coluna Widget Number','2016-01-12 17:02:22','2016-01-27 10:45:25','sergio@trend2b.com','3',1),(4,'Menu Admin','Visualização do Menu Admin','2016-01-13 10:34:21','2016-01-26 13:11:18','sergio@trend2b.com','3',0),(5,'Permissões','Visualização da Página de Permissionamento','2016-01-13 16:51:55','2016-01-13 18:51:55','sergio@trend2b.com','1',1),(6,'Widet Valores','Visualização do Widget Valores','2016-01-13 16:53:33','2016-01-13 18:53:33','sergio@trend2b.com','1',1),(7,'Botão Excluir','Permite excluir uma entidade especifica','2016-01-14 08:57:38','2016-01-14 10:57:38','augusto@trend2b.com','1',1),(8,'Marketing','Permite ver a aba de marketing','2016-01-14 16:45:29','2016-01-14 18:45:29','augusto@trend2b.com','1',1),(9,'Marketing Felipe','Permite ver a aba de marketing do felipe','2016-01-14 17:43:11','2016-01-14 19:43:11','augusto@trend2b.com','1',1),(10,'teste','teste de insert','2016-01-19 18:38:24','2016-01-19 20:38:24','pegaDoCookie@trend2.com','4',1),(11,'Teste Toaster','Teste de toaster padrãozao zaooo','2016-01-19 18:48:06','2016-01-27 11:13:35','augusto@trend2b.com','4',1),(12,'eqweqwe','qweqweqw','2016-01-19 18:53:04','2016-01-19 20:53:04','pegaDoCookie@trend2.com','4',1),(13,'teeeeete','Permite que o usuário pague a meta na aba METSO','2016-01-19 18:59:09','2016-01-27 12:48:57','augusto@trend2b.com','2',0),(14,'fofovcpacagorobilo','teste final da finalidade final','2016-01-19 19:02:41','2016-01-19 21:02:41','pegaDoCookie@trend2.com','3',1),(15,'asdfasfasf','fasfas','2016-01-19 19:02:56','2016-01-19 21:02:56','pegaDoCookie@trend2.com','4',1),(16,'asdfasfasfsdfasddf','fasfasfsdfasdfasd','2016-01-19 19:03:29','2016-01-19 21:03:29','pegaDoCookie@trend2.com','2',1),(17,'testeKUI','tewrwerwerwersdfsda','2016-01-19 19:05:59','2016-01-19 21:05:59','pegaDoCookie@trend2.com','4',1),(18,'sdfgkçertyuo','dfjhlrtyhujikl','2016-01-19 19:06:51','2016-01-19 21:06:51','pegaDoCookie@trend2.com','3',1),(19,'testerrrrrrrrr','wddfwesrfsdfgsdfg','2016-01-20 09:10:37','2016-01-20 11:10:37','pegaDoCookie@trend2.com','1',1),(20,'hjdjslpoopopop','popop','2016-01-20 09:40:27','2016-01-27 13:27:43','pegaDoCookie@trend2.com','2',0),(21,'157784','4658464646','2016-01-20 09:41:23','2016-01-26 15:46:14','pegaDoCookie@trend2.com','4',0),(22,'dsadasd','asdasdasd','2016-01-20 09:44:00','2016-01-20 11:44:00','pegaDoCookie@trend2.com','4',1),(23,'sadfadffasdafsd','dffasdfsdfa','2016-01-20 09:44:39','2016-01-20 11:44:39','pegaDoCookie@trend2.com','4',1),(24,'dgsvdfasdfa','asddfasdfas','2016-01-20 15:50:45','2016-01-20 17:50:45','sergio@trend2b.com','4',1),(25,'fghjngfhf','jgjhgf','2016-01-22 18:03:39','2016-01-22 20:03:39','sergio@trend2b.com','4',1),(26,'bhlggfdf','fgfgfgdfdf','2016-01-26 09:29:38','2016-01-26 11:29:38','sergio@trend2b.com','4',1),(27,'hjdghsdjk','sadfsdafsadfasdf','2016-01-26 09:42:07','2016-01-27 13:44:35','sergio@trend2b.com','4',0),(28,'klkl78978979','assdasd','2016-01-26 09:43:26','2016-01-26 11:43:26','sergio@trend2b.com','1',1),(29,'fdgdfgdsf','sdfgsdfgsd','2016-01-26 09:49:10','2016-01-26 11:49:10','sergio@trend2b.com','3',1),(30,'17417414174174','7411744174','2016-01-26 09:51:22','2016-01-26 11:51:22','sergio@trend2b.com','3',1),(31,'4563122312312313123','3122132133246549879','2016-01-26 09:55:46','2016-01-26 15:51:51','sergio@trend2b.com','3',0),(32,'123456789','463789546','2016-01-26 09:56:37','2016-01-26 11:56:37','sergio@trend2b.com','1',1),(33,'adfefddf','dfdfddsdfdfdf','2016-01-26 09:59:46','2016-01-26 11:59:46','sergio@trend2b.com','1',1),(34,'89f897df987df897df987','978df897sdf89sd987sdf897','2016-01-26 10:08:04','2016-01-26 12:08:04','sergio@trend2b.com','3',1),(35,'lçghciouhcjufjhg','ffgdsffsdfsfgfsd','2016-01-26 10:11:05','2016-01-26 12:11:05','sergio@trend2b.com','1',1),(36,'sdsdsdfsdfssd','ssdfsdsdfssddsf','2016-01-26 10:12:20','2016-01-26 12:12:20','sergio@trend2b.com','1',1),(37,'jhsdfgsd','jhdsjkdsfkdsfhkgsdkjfhgsdk','2016-01-26 10:42:21','2016-01-26 12:42:21','sergio@trend2b.com','3',1);
/*!40000 ALTER TABLE `resources` ENABLE KEYS */;
UNLOCK TABLES;
ALTER DATABASE `philips_dv` CHARACTER SET utf8 COLLATE utf8_general_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`bd_trend2b_sa`@`%`*/ /*!50003 TRIGGER trigger_add_resources_to_group_trend2badmin
AFTER INSERT ON resources FOR EACH ROW

BEGIN

INSERT INTO role_group_resources (id_group, id_resource, owner)
VALUES (2, NEW.id_resource, NEW.owner);

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE `philips_dv` CHARACTER SET utf8 COLLATE utf8_bin ;

--
-- Table structure for table `resources_overrides`
--

DROP TABLE IF EXISTS `resources_overrides`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resources_overrides` (
  `id_resource_override` int(10) NOT NULL AUTO_INCREMENT,
  `id_user` int(10) NOT NULL,
  `id_resource` int(10) NOT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `owner` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_resource_override`),
  UNIQUE KEY `unique_id_user_id_resource` (`id_user`,`id_resource`),
  KEY `fk_user_resources_overrides` (`id_user`),
  KEY `fk_resource_resources_overrides` (`id_resource`),
  CONSTRAINT `fk_resource_resources_overrides` FOREIGN KEY (`id_resource`) REFERENCES `resources` (`id_resource`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_user_resources_overrides` FOREIGN KEY (`id_user`) REFERENCES `oauth_users` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resources_overrides`
--

LOCK TABLES `resources_overrides` WRITE;
/*!40000 ALTER TABLE `resources_overrides` DISABLE KEYS */;
INSERT INTO `resources_overrides` VALUES (1,1,3,'2016-01-12 17:21:29','2016-01-21 12:33:36','sergio@trend2b.com',1);
/*!40000 ALTER TABLE `resources_overrides` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resources_type`
--

DROP TABLE IF EXISTS `resources_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resources_type` (
  `id_type` varchar(20) NOT NULL,
  `type_name` varchar(20) NOT NULL,
  PRIMARY KEY (`id_type`),
  UNIQUE KEY `unique_resource_name` (`type_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resources_type`
--

LOCK TABLES `resources_type` WRITE;
/*!40000 ALTER TABLE `resources_type` DISABLE KEYS */;
INSERT INTO `resources_type` VALUES ('3','Atualização'),('1','Criação'),('4','Exclusão'),('2','Leitura');
/*!40000 ALTER TABLE `resources_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_group`
--

DROP TABLE IF EXISTS `role_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_group` (
  `id_group` int(10) NOT NULL AUTO_INCREMENT,
  `groupname` varchar(80) NOT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `owner` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_group`),
  UNIQUE KEY `groupname` (`groupname`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_group`
--

LOCK TABLES `role_group` WRITE;
/*!40000 ALTER TABLE `role_group` DISABLE KEYS */;
INSERT INTO `role_group` VALUES (1,'Admin','2016-01-12 16:45:13','2016-01-12 18:45:13','sergio@trend2b.com',1),(2,'Trend2bAdmin','2016-01-13 16:40:04','2016-01-13 18:40:04','sergio@trend2b.com',1);
/*!40000 ALTER TABLE `role_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_group_resources`
--

DROP TABLE IF EXISTS `role_group_resources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_group_resources` (
  `id_group_resource` int(10) NOT NULL AUTO_INCREMENT,
  `id_group` int(10) NOT NULL,
  `id_resource` int(10) NOT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `owner` varchar(255) NOT NULL,
  PRIMARY KEY (`id_group_resource`),
  UNIQUE KEY `unique_id_group_id_resource` (`id_group`,`id_resource`),
  KEY `fk_group_role_group_resources` (`id_group`),
  KEY `fk_resources_role_group_resources` (`id_resource`),
  CONSTRAINT `fk_group_role_group_resources` FOREIGN KEY (`id_group`) REFERENCES `role_group` (`id_group`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_resources_role_group_resources` FOREIGN KEY (`id_resource`) REFERENCES `resources` (`id_resource`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_group_resources`
--

LOCK TABLES `role_group_resources` WRITE;
/*!40000 ALTER TABLE `role_group_resources` DISABLE KEYS */;
INSERT INTO `role_group_resources` VALUES (1,1,1,'2016-01-12 17:03:34','2016-01-12 19:03:34','sergio@trend2b.com'),(2,1,2,'2016-01-12 17:03:50','2016-01-12 19:03:50','sergio@trend2b.com'),(3,1,4,'2016-01-13 10:36:57','2016-01-13 12:36:57','sergio@trend2b.com'),(4,2,1,'2016-01-13 16:41:54','2016-01-13 18:41:54','sergio@trend2b.com'),(5,2,2,'2016-01-13 16:41:54','2016-01-13 18:41:54','sergio@trend2b.com'),(6,2,3,'2016-01-13 16:41:55','2016-01-13 18:41:55','sergio@trend2b.com'),(7,2,4,'2016-01-13 16:41:55','2016-01-13 18:41:55','sergio@trend2b.com'),(8,2,5,'2016-01-13 16:51:55','2016-01-13 18:51:55','sergio@trend2b.com'),(9,2,6,'2016-01-13 16:53:33','2016-01-13 18:53:33','sergio@trend2b.com'),(10,2,7,'2016-01-14 08:57:38','2016-01-14 10:57:38','augusto@trend2b.com'),(11,2,8,'2016-01-14 16:45:29','2016-01-14 18:45:29','augusto@trend2b.com'),(12,2,9,'2016-01-14 17:43:11','2016-01-14 19:43:11','augusto@trend2b.com'),(13,2,10,'2016-01-19 18:38:24','2016-01-19 20:38:24','pegaDoCookie@trend2.com'),(14,2,11,'2016-01-19 18:48:06','2016-01-19 20:48:06','pegaDoCookie@trend2.com'),(15,2,12,'2016-01-19 18:53:04','2016-01-19 20:53:04','pegaDoCookie@trend2.com'),(16,2,13,'2016-01-19 18:59:09','2016-01-19 20:59:09','pegaDoCookie@trend2.com'),(17,2,14,'2016-01-19 19:02:41','2016-01-19 21:02:41','pegaDoCookie@trend2.com'),(18,2,15,'2016-01-19 19:02:56','2016-01-19 21:02:56','pegaDoCookie@trend2.com'),(19,2,16,'2016-01-19 19:03:29','2016-01-19 21:03:29','pegaDoCookie@trend2.com'),(20,2,17,'2016-01-19 19:05:59','2016-01-19 21:05:59','pegaDoCookie@trend2.com'),(21,2,18,'2016-01-19 19:06:51','2016-01-19 21:06:51','pegaDoCookie@trend2.com'),(22,2,19,'2016-01-20 09:10:37','2016-01-20 11:10:37','pegaDoCookie@trend2.com'),(23,2,20,'2016-01-20 09:40:27','2016-01-20 11:40:27','pegaDoCookie@trend2.com'),(24,2,21,'2016-01-20 09:41:23','2016-01-20 11:41:23','pegaDoCookie@trend2.com'),(25,2,22,'2016-01-20 09:44:00','2016-01-20 11:44:00','pegaDoCookie@trend2.com'),(26,2,23,'2016-01-20 09:44:39','2016-01-20 11:44:39','pegaDoCookie@trend2.com'),(27,2,24,'2016-01-20 15:50:45','2016-01-20 17:50:45','sergio@trend2b.com'),(28,2,25,'2016-01-22 18:03:39','2016-01-22 20:03:39','sergio@trend2b.com'),(29,2,26,'2016-01-26 09:29:38','2016-01-26 11:29:38','sergio@trend2b.com'),(30,2,27,'2016-01-26 09:42:07','2016-01-26 11:42:07','sergio@trend2b.com'),(31,2,28,'2016-01-26 09:43:26','2016-01-26 11:43:26','sergio@trend2b.com'),(32,2,29,'2016-01-26 09:49:10','2016-01-26 11:49:10','sergio@trend2b.com'),(33,2,30,'2016-01-26 09:51:22','2016-01-26 11:51:22','sergio@trend2b.com'),(34,2,31,'2016-01-26 09:55:46','2016-01-26 11:55:46','sergio@trend2b.com'),(35,2,32,'2016-01-26 09:56:37','2016-01-26 11:56:37','sergio@trend2b.com'),(36,2,33,'2016-01-26 09:59:46','2016-01-26 11:59:46','sergio@trend2b.com'),(37,2,34,'2016-01-26 10:08:04','2016-01-26 12:08:04','sergio@trend2b.com'),(38,2,35,'2016-01-26 10:11:05','2016-01-26 12:11:05','sergio@trend2b.com'),(39,2,36,'2016-01-26 10:12:20','2016-01-26 12:12:20','sergio@trend2b.com'),(40,2,37,'2016-01-26 10:42:21','2016-01-26 12:42:21','sergio@trend2b.com');
/*!40000 ALTER TABLE `role_group_resources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test_list`
--

DROP TABLE IF EXISTS `test_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_list` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(254) NOT NULL,
  `number` varchar(254) NOT NULL,
  `custom` varchar(254) NOT NULL,
  `created_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test_list`
--

LOCK TABLES `test_list` WRITE;
/*!40000 ALTER TABLE `test_list` DISABLE KEYS */;
INSERT INTO `test_list` VALUES (1,'teste1','456123248','51',NULL,'2016-01-08 11:11:32'),(2,'teste2','5462186541','37',NULL,'0000-00-00 00:00:00'),(3,'teste3','123456845','100',NULL,'0000-00-00 00:00:00'),(4,'teste4','4568786456','30',NULL,'0000-00-00 00:00:00'),(5,'teste5','723484783748','71','2016-01-08 09:11:50','2016-01-08 11:13:21'),(6,'teste6','673838638','110','2016-01-08 09:13:42','2016-01-08 11:13:42'),(7,'teste7','7894632155','17','2016-01-08 09:31:37','2016-01-08 11:36:08');
/*!40000 ALTER TABLE `test_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `vw_all_resources`
--

DROP TABLE IF EXISTS `vw_all_resources`;
/*!50001 DROP VIEW IF EXISTS `vw_all_resources`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_all_resources` AS SELECT 
 1 AS `id_resource`,
 1 AS `resource_name`,
 1 AS `resource_desc`,
 1 AS `created_on`,
 1 AS `updated_on`,
 1 AS `owner`,
 1 AS `id_type`,
 1 AS `type_name`*/;
SET character_set_client = @saved_cs_client;

--
-- Dumping events for database 'philips_dv'
--

--
-- Dumping routines for database 'philips_dv'
--
/*!50003 DROP FUNCTION IF EXISTS `fn_ParImpar` */;
ALTER DATABASE `philips_dv` CHARACTER SET utf8 COLLATE utf8_general_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`bd_trend2b_sa`@`%` FUNCTION `fn_ParImpar`(valor INT) RETURNS varchar(1) CHARSET utf8
    DETERMINISTIC
BEGIN

DECLARE retorno NVARCHAR(1);

  IF(valor%2 = 0) THEN SET retorno = 'P'; 
  ELSE SET retorno = 'I';
  END IF;
  
  RETURN retorno;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE `philips_dv` CHARACTER SET utf8 COLLATE utf8_bin ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_resources_by_userid` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`bd_trend2b_sa`@`%` PROCEDURE `sp_get_resources_by_userid`(
   IN userID   varchar(10))
BEGIN
   DROP TABLE IF EXISTS resources_temp;

   CREATE TEMPORARY TABLE resources_temp
   AS
      (SELECT #ou.id_user
              #,ou.username
              #,ou.id_group
              rgs.id_resource
              #,rt.type_name
       FROM oauth_users ou
            INNER JOIN role_group rg ON rg.id_group = ou.id_group
            INNER JOIN role_group_resources rgs ON rgs.id_group = rg.id_group
            INNER JOIN resources r ON r.id_resource = rgs.id_resource
            LEFT JOIN resources_overrides ro ON ro.id_user = ou.id_user
            INNER JOIN resources_type rt ON rt.id_type = r.id_type
       WHERE ou.id_user = userID
				AND ou.active = 1
                AND r.active = 1);

   DROP TABLE IF EXISTS resources_temp_overrides;

   CREATE TEMPORARY TABLE resources_temp_overrides
   AS
      (SELECT #ou.id_user
              #,ou.username
              #,ou.id_group
              ro.id_resource
              #,rt.type_name
       FROM oauth_users ou
            INNER JOIN resources_overrides ro ON ro.id_user = ou.id_user
            INNER JOIN resources r ON r.id_resource = ro.id_resource
            INNER JOIN resources_type rt ON rt.id_type = r.id_type
            
            WHERE ou.id_user = userID
					AND ou.active = 1
                    AND ro.active = 1
                    AND r.active = 1);
            
   SELECT id_resource AS resource FROM resources_temp #, type_name AS type 
   UNION ALL
   SELECT id_resource AS resource #, type_name AS type
     FROM resources_temp_overrides
   ORDER BY resource;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_ParImpar` */;
ALTER DATABASE `philips_dv` CHARACTER SET utf8 COLLATE utf8_general_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`bd_trend2b_sa`@`%` PROCEDURE `sp_ParImpar`(valor INT)
BEGIN
	SELECT fn_ParImpar(valor);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE `philips_dv` CHARACTER SET utf8 COLLATE utf8_bin ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_TestList` */;
ALTER DATABASE `philips_dv` CHARACTER SET utf8 COLLATE utf8_general_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`bd_trend2b_sa`@`%` PROCEDURE `sp_TestList`(IN id varchar(10))
BEGIN


  SET @qty = (SELECT COUNT(*) FROM test_list t WHERE ((t.id=id) OR (id IS NULL)));
  SELECT @qty;

  IF(@qty > 0) THEN
    
    SELECT t.number, t.id, t.custom, t.name, t.created_on, t.updated_on FROM test_list t
    WHERE ((t.id=id) or (id IS NULL))
    ORDER BY name ASC;
    
  ELSE
	  SIGNAL SQLSTATE '45000'
    SET MESSAGE_TEXT = 'O id não foi encontrado';
    /*CALL O_ID_NAO_FOI_ENCONTRADO();*/
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE `philips_dv` CHARACTER SET utf8 COLLATE utf8_bin ;

--
-- Final view structure for view `vw_all_resources`
--

/*!50001 DROP VIEW IF EXISTS `vw_all_resources`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`bd_trend2b_sa`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_all_resources` AS select `r`.`id_resource` AS `id_resource`,`r`.`resource_name` AS `resource_name`,`r`.`resource_desc` AS `resource_desc`,`r`.`created_on` AS `created_on`,`r`.`updated_on` AS `updated_on`,`r`.`owner` AS `owner`,`rt`.`id_type` AS `id_type`,`rt`.`type_name` AS `type_name` from (`resources` `r` join `resources_type` `rt` on((`rt`.`id_type` = `r`.`id_type`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-29 11:14:54
